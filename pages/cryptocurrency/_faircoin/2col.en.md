---
title: Faircoin
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

![](faircoin-qr.png)

---

# Faircoin
Send your faircoins to our wallet at:
fLtQUZWMBiYkEneKb1hqTDoyoqzCkjL2qY
