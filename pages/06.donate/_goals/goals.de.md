---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Wir teilen die Spenden'
        text: "Wenn wir mindestens 400€ an Spenden erhalten, teilen wir 15 % unseres Überschusses mit den Entwicklern der Software, die wir nutzen. Disroot würde ohne diese Entwickler nicht existieren."
        unlock: yes
    -
        title: 'Eine Aufwands-entschädigung für einen Helfer'
        text: "Wenn wir am Ende des Monats mehr als 140€ übrig haben, zahlen wir einem Mitglied des Disroot-Teams eine Aufwandsentschädigung von 140€."
        unlock: yes
    -
        title: 'Eine Aufwands-entschädigung für zwei Helfer'
        text: "Wenn wir am Ende des Monats mehr als 280€ übrig haben, zahlen wir zwei Mitgliedern des Disroot-Teams eine Aufwandsentschädigung von 140€."
        unlock: yes
    -
        title: 'Eine Aufwands-entschädigung für drei Helfer'
        text: "Wenn wir am Ende des Monats mehr als 420€ übrig haben, zahlen wir drei Mitgliedern des Disroot-Teams eine Aufwandsentschädigung von 140€."
        unlock: yes
    -
        title: 'Eine Aufwands-entschädigung für vier Helfer'
        text: "Wenn wir am Ende des Monats mehr als 560€ übrig haben, zahlen wir vier Mitgliedern des Disroot-Teams eine Aufwandsentschädigung von 140€."
        unlock: no
---

<div class=goals markdown=1>

</div>
