---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Wir sind dankbar für Eure Spenden und Unterstützung!

Durch Eure Unterstützung kommen wir unserem Ziel der finanziellen Zukunftsfähigkeit unseres Projektes immer näher und beweisen, dass ein Modell basierend auf sozialem Zussamenhalt möglich ist. 
