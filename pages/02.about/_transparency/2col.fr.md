---
title: Transparence
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

<br>
![](about-trust.png)

---

# Transparence et ouverture

Chez **Disroot**, nous utilisons des logiciels 100% libres et open source. Cela signifie que le code source (le fonctionnement du logiciel) est accessible au public. N'importe qui est en mesure d'apporter des améliorations ou des ajustements au logiciel et il peut être audité et contrôlé à tout moment - pas de portes dérobées cachées ou d'autres logiciels malveillants.

Nous voulons être totalement transparents et ouverts à l'égard des personnes qui utilisent nos services et, à cet effet, nous publions des informations sur l'état actuel du projet, la situation financière, nos plans et nos idées. Nous aimerions également connaître vos suggestions et vos commentaires afin de pouvoir offrir la meilleure expérience possible à tous les utilisateurs de Disroot.
