---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Disroot Team

Disroot был основан пользователями **Antilopa** и **Muppeth** в 2015 году.
В июле 2019 к Команде присоединились **Fede** и **Meaz**.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz")
