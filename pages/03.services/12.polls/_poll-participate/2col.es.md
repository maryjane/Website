---
title: 'Participar en encuestas'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Participa en la encuesta
Elige tus preferencias sin necesidad de crear una cuenta. **Framadate** permite amplias discusiones dentro de la encuesta para llevar la participación en la decisión más allá de solo marcar tu elección preferida.

---

![](en/poll_participate.gif)
