---
title: 'Crear encuesta'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/poll_results.gif)

---

# Visualiza los resultados
Vuelve a revisar la encuesta para ver el resultado. La vista gráfica hace más sencillo tomar decisiones finales permitiendo que tengas un mejor panorama de todas las elecciones hechas por lxs participantes. El resultado de la encuesta puede descargarse a tu computadora como un archivo csv.
