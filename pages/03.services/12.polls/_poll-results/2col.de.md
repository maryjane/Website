---
title: 'Umfrage erstellen'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](de/poll_results.gif)

---

# Ergebnisse ansehen
Sieh Dir im Nachhinein die Umfrage an, um das Ergebnis zu erfahren. Die grafische Darstellung macht Dir eine abschließende Entscheidung leichter, indem Sie Dir einen besseren Überblick über die Entscheidungen der Umfrage-Teilnehmer verschafft. Die Umfrage-Ergebnisse kannst Du Dir auch als .csv-Datei lokal herunterladen.
