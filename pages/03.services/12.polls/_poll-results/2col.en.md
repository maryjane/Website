---
title: 'Create poll'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/poll_results.gif)

---

# View results
Check back on the poll to see the outcome. The graph view makes it easier to make final decission by allowing you to have better overview of all choices made by participants. The poll result can be downloaded as a csv file to your computer.
