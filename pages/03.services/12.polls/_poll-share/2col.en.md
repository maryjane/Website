---
title: 'Share poll'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](poll_share.gif)

---

# Share poll
Share your poll with participants by sending them a link.
