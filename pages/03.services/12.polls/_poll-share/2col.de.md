---
title: 'Umfrage teilen'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](poll_share.gif)

---

# Umfrage teilen
Teile Deine Umfrage mit den ausgesuchten Teilnehmern, indem Du ihnen einfach einen Link zusendest.
