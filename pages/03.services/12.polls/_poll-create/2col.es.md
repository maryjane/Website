---
title: 'Crear encuesta'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Crear encuesta

Posibilidad de crear encuestas por fecha específica o una de tipo más general.
---

![](en/poll_create.gif)
