---
title: 'Create poll'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Create poll

Possibility to create date specific or a more general type of poll.

---

![](en/poll_create.gif)
