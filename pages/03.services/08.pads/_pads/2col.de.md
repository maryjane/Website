---
title: Pads
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Ein Pad öffnen</a>
<a class="button button1" href="https://calc.disroot.org/">Tabellenkalkulation erstellen</a>

---

![](etherpad.png)
## Gemeinschaftliches Bearbeiten in wirklicher Echtzeit
Disroots Pads basiert auf **Etherpad**. Ein Pad ist ein Onlinetext, den Du gemeinschaftlich mit anderen bearbeiten kannst, in Echtzeit, direkt im Browser. Die Änderungen jedes Benutzers werden sofort auf dem Bildschirm dargestellt.

Schreibe Artikel, Pressemitteilungen, To-Do-Listen, etc. zusammen mit Deinen Freunden, Mitstudenten oder Kollegen.

Du benötigst keinen Disroot-Account, um diesen Service zu nutzen.

Du kannst [**Padland**](https://f-droid.org/de/packages/com.mikifus.padland/) auf Android nutzen, um Deine Disroot-Pads direkt auf Deinem Android-Gerät zu öffnen oder zu erstellen.


Disroot Pads: [https://pad.disroot.org](https://pad.disroot.org)

Projekt-Website: [http://etherpad.org](http://etherpad.org)

Quellcode: [https://github.com/ether/etherpad-lite](https://github.com/ether/etherpad-lite)


![](ethercalc.png)
## Gemeinschaftliche Online-Tabellenkalkulation
Disroots Spreadsheets basieren auf **Ethercalc**. Du kannst eine Online-Tabellenkalkulation erstellen, die Du gemeinschaftlich mit anderen bearbeiten kannst, in Echtzeit, direkt im Browser. Die Änderungen jedes Benutzers werden sofort auf dem Bildschirm dargestellt.

Arbeitet zusammen an Inventuren, Umfrageformularen, Listenverwaltungen, etc. und mehr!

Du benötigst keinen Disroot-Account, um diesen Service zu nutzen.

Disroot Calc: [https://calc.disroot.org](https://calc.disroot.org)

Projekt-Website: [https://ethercalc.net](https://ethercalc.net)

Quellcode: [https://github.com/audreyt/ethercalc](https://github.com/audreyt/ethercalc)
