---
title: Upload
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://upload.disroot.org/">Uploader des fichiers</a>

---

![](lufi.png)
## Lufi - Service de téléchargement de fichiers temporaires cryptés

Le service Upload de Disroot est un logiciel d'hébergement de fichiers, propulsé par **Lufi**. Il stocke temporairement des fichiers afin que vous puissiez les partager avec d'autres personnes en utilisant un lien.
Pour protéger la confidentialité, tous les fichiers sont cryptés dans le navigateur lui-même - Cela signifie que vos fichiers ne quittent jamais votre ordinateur non cryptés. Les administrateurs ne pourront pas voir le contenu de votre fichier, ni votre administrateur réseau ou votre FAI. Les administrateurs peuvent seulement voir le nom du fichier, sa taille et son type (quel type de fichier c'est : vidéo, texte, etc.).

Code source : [https://framagit.org/fiat-tux/hat-softwares/lufi](https://framagit.org/fiat-tux/hat-softwares/lufi)
