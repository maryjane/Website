---
title: 'Taiga caratteritiche'
wider_column: left
---

## Pannello Kanban

Per coloro che non hanno familiarità con gli strumenti di project management e scrum, si consiglia di scegliere un semplice modello Kanban. (Scopri di più su Kanban )


## Progetto Scrum

Con backlog, pianificazione degli sprint, user story points e tutte le altre cose di cui ogni progetto Scrum ha bisogno ( Per saperne di più su SCRUM )


## Pannello problematiche

Luogo in cui è possibile raccogliere tutte le questioni, i suggerimenti, i problemi ecc. e lavorare collettivamente per risolverli.


## Wiki

Documenta il tuo lavoro con il wiki integrato.


---

![](en/taiga-backlog.png?lightbox=1024)

![](en/taiga-kanban.png?lightbox=1024)
