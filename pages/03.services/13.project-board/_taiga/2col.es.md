---
title: Taiga
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Inscribirse en Disroot</a>
<a class="button button1" href="https://board.disroot.org/">Iniciar sesión</a>

---
![taiga_logo](taiga.png)

## Tablero de gestión de proyecto

El **tablero de proyectos** de **Disroot** es una herramienta de gestión de proyectos, desarrollada por **Taiga** que trabaja con el método de [desarrollo ágil](https://es.wikipedia.org/wiki/Desarrollo_%C3%A1gil_de_software#M%C3%A9todos_%C3%A1giles) en mente. Genera un claro y visual resumen general del estado actual de tu proyecto para cualquier persona involucrada. Hace la planificación muy sencilla y te mantiene a ti y a tu equipo enfocados en las tareas. Simplemente crea un proyecto, invita a lxs miembros de tu equipo, creen tareas y colóquenlas en el tablero. Elijan quien tomará la responsabilidad por las tareas, sigan el progreso, comenten, decidan y vean crecer su proyecto.

Tablero de Disroot: [https://board.disroot.org](https://board.disroot.org)

Página del proyecto: [https://taiga.io/](https://taiga.io/)

Código fuente: [https://github.com/taigaio](https://github.com/taigaio)
