---
title: Taiga
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Disroot-Account registrieren</a>
<a class="button button1" href="https://board.disroot.org/">Anmelden</a>

---

![taiga_logo](taiga.png)

## Projektmanagement

Disroots Projektboard ist ein Projektmanagement-Tool auf der Basis von **Taiga**. Bei der Entwicklung wurde dabei besonders auf eine flexible und bewegliche Methodik wert gelegt. Taiga kann dadurch bei nahezu jedem Projekt und jeder Gruppenarbeit eingesetzt werden, auch außerhalb der IT-Sphäre. Es erzeugt einen klaren, sichtbaren Überblick über den derzeitigen Status Deines Projekts für jeden, der damit befasst ist. Es erleichtert spürbar die Planung und ermöglicht Dir und Deinem Team, Euch auf Eure tatsächlichen Aufgaben zu konzentrieren. Taiga kann durch seine Flexibilität an jedes Projekt angepasst werden, von komplexen Entwicklungsprojekten bis hin zu grundlegenden Haushaltsaufgaben. Deine Fantasie ist das Limit.
Wenn Du noch nie ein solches Programm genutzt hast, wirst Du überrascht sein, wie sehr Taiga Dein Leben verbessern kann. Erstelle einfach ein Projekt, lade Gruppenmitglieder ein, erstelle Aufgaben und setze sie auf das Board. Entscheide, wer die Verantwortung für die Aufgaben übernehmen soll, verfolge den Entwicklungsprozess, kommentiere, entscheide und beobachte, wie Dein Projekt gedeiht.

Disroot-Board: [https://board.disroot.org](https://board.disroot.org)

Projekt-Website: [https://taiga.io/](https://taiga.io/)

Quellcode: [https://github.com/taigaio](https://github.com/taigaio)
