---
title: "Lien vers un domaine personnel"
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
section_id: alias
---

## Lien vers un domaine personnel
<br>
<a class="button button2" href="https://disroot.org/perks">Demander un avantage</a>

---

<br>
Cette fonction est un avantage donné en remerciement pour votre contribution financière au projet. Elle vous permet d'utiliser votre propre domaine pour pouvoir envoyer et recevoir des courriels. C'est une récompense à vie.
