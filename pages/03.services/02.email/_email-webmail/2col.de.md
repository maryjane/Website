---
title: 'E-Mail Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](de/webmail.png)

---


Du kannst von jedem Gerät (mit Internetzugang) auf Deine E&#8209;Mails zugreifen, indem Du unsere Webmail-Oberfläche unter [https://mail.disroot.org](https://mail.disroot.org) nutzt.

Unsere Webmail haben wir mit **RainLoop** verwirklicht. RainLoop ist ein moderner Ansatz zum Thema Webmail. Es ist simpel, schnell und bietet die meisten Features, die man von Webmail erwarten kann:

- Themes: Wähle Dein Hintergrundbild.
- Threads: Stelle Unterhaltungen im Thread-Modus dar.
- Übersetzungen: Die Benutzeroberfläche gibt es in vielen Sprachen.
- Filter: Mit Hilfe von Regeln verschiebst oder kopierst Du E&#8209;Mails in die richtigen Verzeichnisse, leitest sie weiter oder lehnt sie ab.
- Verzeichnisse: Verwalte Deine Verzeichnisse (Erstellen, Entfernen oder Verstecken).
- Kontakte: Du kannst Deine Kontakte hinzufügen oder sogar Deine Nextcloud-Kontakte direkt mit der Webmail verknüpfen.
- GPG Verschlüsselung: Der **RainLoop**-Webclient bietet integrierte Verschlüsselung.

**HINWEIS**: *Bitte sei Dir bewusst, dass es sich hierbei um eine serverseitige Verschlüsselung handelt. Das heißt, dass Du keine Kontrolle über Deinen geheimen Schlüssel hast. Diese Methode ist nicht so sicher wie die Verwendung eines Desktop-Clients, wie zum Beispiel Thunderbird, mit lokaler Verschlüsselung. Andererseits ist es natürlich besser als überhaupt keine Verschlüsselung zu verwenden...*

Projekt-Website: [https://www.rainloop.net](https://www.rainloop.net/)
