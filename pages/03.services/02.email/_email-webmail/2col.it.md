---
title: 'Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](en/webmail.png)

---

<br>

RainLoop è una webmail moderna dall'aspetto semplice ma che offre tutte le impostazioni e le configurazioni che ci si può aspettare da un servizio di posta moderno:
<ul class=disc>
<li>Temi: Possibilità di scegliere tra vari sfondi.</li>
<li>Vista per argomenti: Possibilità di visualizzare i messaggi in ordine di argomenti.</li>
<li>Localizzazione: Possibilità di scegliere tra varie lingue.</li>
<li>Filtri: Possibilità di creare automaticamente dei filtri per spostare, copiare, inoltrare o eliminare i messaggi.</li>
<li>Cartelle: Possibilità di gestire le cartelle (aggiungere, rimuovere o nascondere).</li>
<li>GPG: RainLoop offre la possibilità di criptare i messaggi diretttamente dall'interfaccia web.</li>
</ul>

*Attenzione, l'utilizzo di GPG direttamente dall'interfaccia web significa non avere il pieno controllo sulla propria chiave privata. Questa opzione non è così sicura come l'uso di GPG su un client di posta elettronica come Thunderbird che permette di gestire le proprie chiavi in locale.
