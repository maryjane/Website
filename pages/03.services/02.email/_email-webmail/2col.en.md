---
title: 'Email Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](en/webmail.png)

---

<br>
You can access your mail from any devices by using our webmail at [https://mail.disroot.org](https://mail.disroot.org)

Our webmail is powered by **RainLoop**. Rainloop is a modern approach to webmail. It's simple, fast and provides most of the features one could expect from webmail:

<ul class=disc>
<li>Themes: Select your background picture.</li>
<li>Threads: Display conversations in a threaded mode.</li>
<li>Translations: The interface is being translated to many languages</li>
<li>Filters: Set up rules to automatically move or copy email to specific folders, forward or reject emails.</li>
<li>Folders: Manage your folders (add, remove, or hide).</li>
<li>GPG Encryption: The <b>RainLoop</b> web-client offers built in encryption option.</li>
<li>Contacts: you can add your contacts and even link your Nextcloud contacts directly into the webmail.</li>
</ul>

**NOTE**: *Please be aware that this is a server side encryption, which means you don't have control on your secret key. This is not as safe as using desktop email client, such as Thunderbird, with local encryption. But then again, it is better than not using any encryption at all...*

Project Homepage: [https://www.rainloop.net](https://www.rainloop.net/)
