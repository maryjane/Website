---
title: 'E-Mail-Verschlüsselung'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

## Was ist Verschlüsselung

---


Verschlüsselung bedeutet, dass Du Daten mit einem speziellen Kodier-Prozess so veränderst, dass die Daten unkenntlich werden (sie sind verschlüsselt). Du kannst einen speziellen Dekodier-Prozess anwenden und bekommst die Originaldaten zurück. Indem der Dekodier-Prozess geheimgehalten wird, kann niemand anders die Originaldaten aus den verschlüsselten Daten rekonstruieren.

[YouTube-Video - Wie asymmetrische Verschlüsselung funktioniert (englisch)](https://www.youtube.com/watch?v=E5FEqGYLL0o)
