---
title: 'Caractéristique n.2'
wider_column: left
---

# Interface utilisateur:
L'UI (User Interface) Discourse n'est pas seulement belle mais elle aussi pratique:

- Les notifications dynamiques vous préviendront chaque fois que quelqu'un répondra à votre message, vous citera ou mentionnera votre nom, et si vous n'êtes pas en ligne, vous recevrez une notification par email.
- Finies les discussions qui sont divisées en pages, le fil se révèle pendant que vous faites défiler vers le bas.
- Répondez alors que vous lisez encore. Avec l'écran divisé, vous pouvez toujours lire d'autres réponses ou fils de discussion et ajouter dynamiquement des guillemets, des liens et des références.
- Mises à jour en temps réel. Si un sujet change pendant que vous le lisez ou y répondez, ou si de nouvelles réponses arrivent, il sera mis à jour automatiquement.


# Recherche qui fonctionne vraiment
Cherchez à partir de n'importe quelle page - commencez simplement à taper et obtenez des résultats instantanément au fur et à mesure que vous tapez. Options de recherche avancées pour les utilisateurs expérimentés.

---

![](en/discourse_feature2.png?lightbox=1024)
