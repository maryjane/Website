---
title: Discourse
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">S'inscrire sur Disroot</a>
<a class="button button1" href="https://forum.disroot.org/">Connectez-vous ou inscrivez-vous pour un compte forum seulement</a>

---
![](discourse_logo.png)

## Une approche moderne des forums de discussion.

Le forum de Disroot est alimenté par **Discourse**. Discourse est une approche moderne entièrement open-source des forums de discussion. Il offre tout ce dont votre communauté, groupe ou collectif a besoin pour créer sa plateforme de communication, qu'elle soit publique ou privée. Il donne également la possibilité aux individus de créer des sujets sur des sujets qui les intéressent et de trouver d'autres personnes avec qui en discuter, ou simplement de rejoindre une communauté déjà existante.

Forum Disroot : [https://forum.disroot.org](https://forum.disroot.org)

Page d'accueil du projet : [http://discourse.org](http://discourse.org)

Le code source : [https://github.com/discourse/discourse](https://github.com/discourse/discourse)
