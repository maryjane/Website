---
title: Discourse
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Accedi con l'account Disroot</a>
<a class="button button1" href="https://forum.disroot.org/">Registrati o accedi con un account dedicato solo al forum</a>

---
![](discourse_logo.png)

## Un approccio moderno ai forum di discussione.

Discourse è un software open-source con un approccio moderno ai forum di discussione. Offre tutto quanto una comunità o un collettivo necessita per creare la propria piattaforma di comunicazione. Permette inoltre ai singoli utenti la possibilità di creare topic di proprio interesse e partecipare a discussioni create da altri utenti della comunità.
Può essere utilizzato come:
<ul class=list>
 <li>una mailing list</li>
 <li>un forum di discussione</li>
 <li>una chat permanente</li>
 </ul>


Disroot Forum: [https://forum.disroot.org](https://forum.disroot.org)

Pagina del progetto: [http://discourse.org](http://discourse.org)

Codice sorgente: [https://github.com/discourse/discourse](https://github.com/discourse/discourse)
