---
title: 'Feature 1'
wider_column: right
---

![](de/discourse_feature1.png?lightbox=1024)

---

## Mailingliste und Forum in einem
Neben der vollen Funktionalität als Forum kann Discourse auch als Mailingliste genutzt werden. Du kannst per Email neue Diskussionen erstellen und auf bestehende Diskussionen antworten. Dafür musst Du Dich nicht mal über die Web-Oberfläche anmelden. Du kannst Discourse genau wie eine traditionelle Mailingliste nutzen.
<br>
## Auf Mobilgeräte angepasste Oberfläche
Die Web-Oberfläche ist in einem mobilen Layout gestaltet, so dass Du keine externe App auf Deinem Mobilgerät benötigst.
<br>
## Öffentliche vs. Private Gruppen
Du kannst öffentliche oder private Forengruppen erstellen, die selbstmoderiert sind. Als Gruppenadministrator bist Du in der Lage zu verwalten, wer an Deiner Forengruppe teilnehmen kann.
