---
title: 'Características 1'
wider_column: right
---

![](en/discourse_feature1.png?lightbox=1024)

---

# Lista de correo y foro todo en uno
Además de un completo y funcional foro, **Discourse** puede ser utilizado también como lista de correo. Puedes comenzar nuevos tópicos y responder enviando un correo. No hay necesidad siquiera de iniciar sesión en la interfaz web, puedes usarlo como una lista de correo tradicional.

# Interfaz amigable con el móvil
Con una disposición para móviles incorporada, no necesitas aplicaciones externas.

# Grupos públicos vs. privados
Puedes iniciar grupos de foros públicos o privados que sean auto-moderados. Como administrador o administradora de un grupo podrás gestionar quiénes participarán en tu grupo de foro.
