---
title: 'Intercambio Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-files-comments.png?lightbox=1024)

---

## Control de intercambio y acceso

Puedes compartir archivos con otrxs usuarixs dentro de **Disroot**, crear y enviar enlaces públicos protegidos con contraseña, permitir a otrxs subir archivos a tu nube y recibir notificaciones cuando una usuaria o usuario comparte archivos contigo directamente.
