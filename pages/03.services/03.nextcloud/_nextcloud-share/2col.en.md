---
title: 'Nextcloud Sharing'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-files-comments.png?lightbox=1024)

---

## Sharing and access control

You can share files with other users within **Disroot**, create and send password protected public links, let others upload files to your cloud and get notifications when a user shares files directly with you.
