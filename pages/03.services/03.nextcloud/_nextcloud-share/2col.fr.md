---
title: 'Partage Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-files-comments.png?lightbox=1024)

---

## Partage et contrôle d'accès

L'interface Web simple vous permet de partager des fichiers avec d'autres utilisateurs sur **Disroot**, de créer et d'envoyer des liens publics protégés par mot de passe, de permettre à d'autres utilisateurs de téléverser des fichiers dans votre cloud et d'obtenir des notifications sur votre téléphone et votre bureau lorsqu'un utilisateur sur un autre serveur cloud partage des fichiers directement avec vous. Et vous pouvez faire toutes ces choses à partir du bureau ou des clients mobiles, aussi.
