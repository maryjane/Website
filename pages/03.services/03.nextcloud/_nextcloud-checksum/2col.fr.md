---
title: 'Somme de contrôle Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Application Somme de contrôle / Checksum

Permet aux utilisateurs de créer une somme de contrôle de hachage d'un fichier.
Les algorithmes possibles sont md5, sha1, sha256, sha384, sha512 and crc32.

---

![](en/checksum.gif)
