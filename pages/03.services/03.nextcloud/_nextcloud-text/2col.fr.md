---
title: 'Nextcloud Text'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Text

Créez des fichiers markdown (.md), et modifiez-les directement en ligne avec aperçu en direct. Vous pouvez aussi les partager et les rendre modifiable par d'autres personnes.

---

![](en/nextcloud-text.png?lightbox=1024)
