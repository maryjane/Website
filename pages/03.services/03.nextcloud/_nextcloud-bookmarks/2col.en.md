---
title: 'Bookmarks'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Choose your favorite client'
clients:
    -
        title: Bookmarks
        logo: en/bookmarks_logo.png
        link: https://f-droid.org/en/packages/org.schabi.nxbookmarks/
        text:
        platforms: [fa-android]
---

## Bookmarks

A bookmark manager for **Nextcloud**. Save your favourite pages and sync across devices.

---

![](en/Bookmarks.png?lightbox=1024)
