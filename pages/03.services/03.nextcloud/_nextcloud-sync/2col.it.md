---
title: 'Nextcloud Sync'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Scegli il tuo client preferito'
clients:
    -
        title: Desktop
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-linux, fa-windows, fa-apple]
    -
        title: Mobile
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-android, fa-apple]
    -
        title: Webbrowser
        logo: en/webbrowser.png
        link: https://cloud.disroot.org
        text: 'Access directly from your browser'
        platforms: [fa-linux, fa-windows, fa-apple]

---

## Nextcloud Sync

Accedi e organizza i tuoi dati su qualsiasi piattaforma. Puoi utilizzare i client Desktop, Android o iOS per lavorare con i tuoi file in movimento o sincronizzare le tue cartelle preferite senza soluzione di continuità tra il tuo desktop e i dispositivi portatili.

---


![](en/nextcloud-files.png?lightbox=1024)
