---
title: 'Cliente web de Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/webclient.png)

---

## Accede a Nextcloud desde cualquier lugar

**Nextcloud** es accesible desde cualquier dispositivo a través de tu navegador web. Por lo tanto, puedes usar todas las aplicaciones del servidor directamente desde tu computadora sin instalar nada excepto un navegador.
