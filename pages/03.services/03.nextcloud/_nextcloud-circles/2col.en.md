---
title: 'Nextcloud Circles'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-circles.png?lighbox=1024)

---

## Circles

Create your own groups of users/colleagues/friends to easily share content with. Your circles can be public, private (require invite though visible to others) or secret (require password and invisible).
