---
title: 'Calendarios y Contactos'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Usa tu cliente preferido'
clients:
    -
        title: GOA
        logo: en/goa.png
        link: https://wiki.gnome.org/Projects/GnomeOnlineAccounts
        text: 'GNOME cuentas en línea'
        platforms: [fa-linux]
    -
        title: Kaddressbook
        logo: en/KDE.png
        link: https://kde.org/applications/office/org.kde.kaddressbook
        text:
        platforms: [fa-linux]
    -
        title: Thunderbird
        logo: en/thunderbird.png
        link: https://www.thunderbird.net/
        text:
        platforms: [fa-linux, fa-windows]
    -
        title: DAVx⁵
        logo: en/davx5.png
        link: https://f-droid.org/en/packages/at.bitfire.davdroid/
        text:
        platforms: [fa-android]


---

## Calendarios y Contactos

Comparte tus calendarios con otrxs usuarixs o grupos de **Nextcloud** en tu servidor o proveedor, fácil y rápidamente. Almacena tus contactos y compártelos entre tus dispositivos para tener siempre acceso a ellos, independientemente del equipo en el que estés.

---

![](en/nextcloud-cal.png?lightbox=1024)
