---
title: Cryptpad
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://cryptpad.disroot.org/register/">Sign up</a>
<a class="button button1" href="https://cryptpad.disroot.org/">Create Pads</a>

---
![](cryptpad_logo.png)

## Cryptpad
Disroot's Cryptpad is powered by Cryptpad. It provides a totally end-to-end encrypted collaborative office suite. It allows you to create, share and work together on text documents, spreadsheets, presentations, whiteboards or organize your project on a kanban board. All this with zero knowledge where data is encrypted before it leaves your computer.

Disroot Cryptpad: [cryptpad.disroot.org](https://cryptpad.disroot.org)

Project homepage: [https://cryptpad.fr](https://cryptpad.fr)

Source code: [https://github.com/xwiki-labs/cryptpad](https://github.com/xwiki-labs/cryptpad)
