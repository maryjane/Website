---
title: 'Kanban'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](todo.png)

## Verschlüsselte, einfache Todo-Listen und Kanban-Boards
Einfaches Erstellen, Teilen und Organisieren Deiner verschlüsselten Todo-Listen und Deiner Kanban-Projektboards!


---
![](whiteboard.png)

## Whiteboards
Verschlüsselte Whiteboards ermöglichen es Dir, zusammen mit anderen in Echtzeit zu zeichnen. Alles Ende-zu-Ende verschlüsselt!
