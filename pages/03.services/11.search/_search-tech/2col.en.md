---
title: Search
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: left
---

# How does it work?

**Searx** does not keep or create index of the webistes by itself. When you type your query into the search box, **Searx** relays this query to a number of other search engines like Google, DuckDuckGo, Bing, etc., and returns results from those engines back to you in aggregated form.

**Searx** may not offer you as personalized results as Google, but that's because it doesn't generate a profile about you nor it shares any personal details about you, your location, or computer with any of the search engines it relays your queries to. This offers much better privacy and acts as a 'shield' from the big, corporate engines spying on you.

You can see [here](https://search.disroot.org/preferences) what search engines can be used to get results on a request.
