---
title: 'Rule'
bgcolor: '#8EB726'
fontcolor: '#FFF'
text_align: center
---
<br>
## Disroot speichert Deine IP-Adresse nicht!
*Nutze den Button oben auf dieser Seite, um das Disroot-Suchplugin in Deinem Firefox zu installieren.*
