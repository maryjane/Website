---
title: 'Clientes para Jitsi'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: Jitsi Web
        logo: jitsi_circle.png
        link: https://calls.disroot.org
        text:
        platforms: [fa-windows, fa-linux, fa-apple]
    -
        title: Jitsi Escritorio
        logo: ../_calls/jitsi_logo.png
        link: https://desktop.jitsi.org/Main/Download#stableline
        text:
        platforms: [fa-windows, fa-linux, fa-apple]

    -
        title: Jitsi Android
        logo: android_logo.png
        link: https://f-droid.org/es/packages/org.jitsi.meet/
        text:
        platforms: [fa-android]

    -
        title: Jitsi Apple
        logo: apple_logo.png
        link: https://apps.apple.com/in/app/jitsi-meet/id1165103905
        text:
        platforms: [fa-apple]


---

<div class=clients markdown=1>

</div>
