---
title: 'Déclaration de mission'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

---

### Les ambitions

La Fondation Disroot.org a des ambitions qui vont au-delà du simple fonctionnement de la plate-forme. L'intention n'est pas de devenir une plate-forme centralisée, mais de promouvoir et de soutenir un écosystème qui aide les autres à établir leurs propres nœuds indépendants. L'un des objectifs à long terme les plus importants est d'offrir un espace à d'autres organisations, individus et collectifs qui veulent commencer leur voyage pour ne pas avoir à réinventer la roue encore et encore. Nous voulons encourager l'émergence de prestataires de services plus indépendants et décentralisés qui travaillent en coopération - et non en concurrence - les uns avec les autres. Ensemble, nous pouvons bâtir un réseau décentralisé vraiment plus fort.

Nous encourageons ouvertement la naissance de nouveaux nœuds fédérés et espérons pouvoir aider d'autres groupes à mettre sur pied leur plateforme Disroot. Afin de soutenir de nouveaux projets ayant des objectifs similaires, nous travaillons sur un ensemble d'outils pour faciliter la mise en œuvre, la maintenance et l'administration des services. Une autre de nos ambitions est de créer un espace de partage de compétences et d'échange de connaissances, de créer une plateforme d'aide juridique pour aider les autres à gérer les règles, les lois, les règlements et l'application de la loi, de créer de nouveaux nœuds et de contribuer au développement des logiciels libres et open source.
