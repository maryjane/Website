---
title: Mission Statement
bgcolor: '#1F5C60'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _root
            - _title
            - _ambition
            - _practical
            - _tldr
body_classes: modular
header_image: surfers.png
---
