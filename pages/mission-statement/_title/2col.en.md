---
title: Mission Statement
bgcolor: '#FFF'
fontcolor: '#1F5C60'
wider_column: right
---

---

## Mission of Disroot.org Foundation

<br>
The mission set by the Disroot.org foundation aims to recover the Internet as the diverse, independent tool it was always meant to be.

The once decentralized, democratic and free internet, has been dominated by a handful of technology giants, promoting concentration in monopolies, more government control and more restrictive regulations. Everything that, in our opinion, opposes and destroys the true essence of this wonderful tool.

By promoting the use of free (libre) and open source software we support collective and community effort towards collaboration. Disroot.org wishes to be an example that there are possible alternatives to the corporate world that is offered in large. Alternatives that are not built on the basis of obtaining economic benefits. Alternatives for which transparency, openness, tolerance and respect are key elements.

### The platform

Disroot.org offers a web services platform built on the principles of privacy, openness, transparency and freedom. Its most important objective from the technical point of view is to provide the best possible service with the best level of support. We chose a working approach in which users (from now on referred to as Disrooters) are the most valuable part and the main beneficiaries of the project; decisions are made with them in mind and not profiting with economic benefit, personal fulfillment, control or power. Disroot.org is committed to defend these key principles in the best possible way.

Online privacy is constantly compromised by those seeking profit and financial control. Our data has become a very valuable commodity and most online companies take advantage of it openly or surreptitiously. Disroot.org is committed to never sell, help process or use any information provided by Disrooters to third parties for any financial, political or power gaining purposes. Disrooters’ data belongs to them and the project simply stores it for those Disrooters to use. Our motto is "The less we know about our users, the better". We implement data encryption whenever possible to ensure that obtaining user data by unauthorized third parties is as difficult as possible and we maintain only the minimum of user logs or data that are essential for the service performance.
