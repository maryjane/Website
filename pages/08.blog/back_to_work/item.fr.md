---
title: 'Retournons au travail'
date: '04-09-2019 11:30'
media_order: coffee.jpg
taxonomy:
  category: news
  tag: [disroot, news, anniversary, birthday]
body_classes: 'single single-post'

---
*(Sprint 43)*
<br><br>
Salut à tous. Bien reposés ? Vous avez mangé de la bonne nourriture, visité des sites ? Chouette. Maintenant, au travail !
<br><br>
Ces dernières semaines, comme nous le mentionnions dans les billets de blog précédents, ont été riches en travail. Au point où nous nous sommes rendu compte que nous prenions beaucoup trop de choses sur nos épaules, ce qui signifie que nous avions tendance à commencer les choses mais ne les finissons pas à l'heure. Maintenant, peu à peu, nous nous arrangeons pour finir les choses que nous avons commencées, alors voici un petit aperçu des choses dont nous nous sommes occupés récemment. Ahhhh, et ne vous inquiétez pas, nous avons aussi eu du temps libre.

# Nouvelles Traductions

Au cours des dernières semaines, nous avons testé plusieurs logiciels de traduction, mais nous n'avons rien trouvé qui réponde à nos besoins. Un site Web et de la documentation sont beaucoup plus difficiles à traduire à l'aide d'un logiciel dédié, contrairement aux applications, et nécessitent de nombreuses bidouilles pour très peu d'avantages. Nous avons donc décidé de continuer le travail de traduction avec Git. Cela pourrait paraître plus difficile à certains, mais l'apprentissage des bases de Git et Markdown est une compétence intéressante à avoir.

Nous avons commencé à mettre en place une nouvelle procédure pour créer, éditer et traduire les howtos et les tutoriels. Nous avons également créé un très bel aperçu de l'état actuel des traductions et un tableau de Taiga, sur lequel même plusieurs personnes travaillant sur une même langue peuvent coordonner leur travail. Pour le moment, cela fonctionne très bien. Nous continuons de faire des ajustements, d'affiner la documentation et d'améliorer le processus, mais nous sommes sur la bonne voie pour avoir et fournir tous les guides et tutoriels dans autant de langues que possible.

Ces dernières semaines, nous avons commencé à traduire le site et quelques howtos en **russe** et **allemand**, et des révisions et mises à jour en **français**, **italien** et **espagnol**. Le plus grand mérite de ces progrès revient aux disrooters **shadowsword**, **wisbit, l3o** et **keyfear**, qui se sont beaucoup impliqués et ont contribué non seulement aux traductions, mais aussi grâce à leurs retours et leur approche collaborative.

Si vous voulez entrer en contact avec l'équipe et aider avec les traductions ou les howtos, rejoignez notre salle XMPP : howto@chat.disroot.org

 - [Tableau des traductions](https://board.disroot.org/project/fede-disroot-translations/epics)
 - [Howto board](https://board.disroot.org/project/disroot-disroot-h2/epics)

# Nouveau thème howto

Au cours des dernières semaines **Antilopa** a été occupé à créer un nouveau thème pour [https://howto.disroot.org](https://howto.disroot.org). Il y a quelque temps, nous avons remarqué que l'ancien thème n'était pas adapté à nos besoins car il devenait de plus en plus difficile de trouver une information à mesure que le nombre de tutoriels augmentait. Comme toujours, le résultat est impressionnant. **Antilopa** a créé un thème très sympathique, minimaliste et épuré qui ressemble à l'apparence et au style de notre site Web. Il y a encore quelques problèmes qui nous ont été signalés concernant le rendu du nouveau site sur certains écrans et **Antilopa** travaille pour le corriger. Si vous trouvez quoi que ce soit, veuillez le signaler sur notre **liste de problème**  [ici](https://board.disroot.org/project/disroot-disroot/issues). N'oubliez pas d'ajouter le préfixe *[WEBSITE]* dans le titre du problème pour qu'il puisse être recherché plus rapidement et corrigé.

# Élaboration de certaines procédures internes

**Meaz** a été très occupé par la file d'attente de notre service de support qui devenait désordonné comme une chambre d'étudiant après une semaine de fête à la maison. Il en résulte une amélioration et une plus grande clarté des files d'attente de demandes, ainsi que l'introduction de quarts de travail. Chaque semaine, une personne différente est responsable de l'attribution des billets, du suivi des billets qui arrivent ainsi que de botter le cul de ceux qui sont trop laxistes sur leur temps de réponse (clin d'œil vous savez qui vous êtes...).
<br><br>
**Fede** a rédigé un joli document de bienvenue que nous mettrons bientôt sur le site web. Il devrait faciliter l'intégration des nouveaux utilisateurs. Nous avons également entamé de longues discussions sur ce que nous pouvons améliorer pour permettre aux nouveaux utilisateurs de s'y retrouver facilement. Nous savons qu'il y a beaucoup de choses à améliorer. Notre objectif est de travailler sur une expérience plus homogène dans tous les services. Nous avons passé un certain temps à réfléchir et à définir certains des éléments les plus importants et nous passerons à leur mise en œuvre dans les sprints à venir.

# FAQ

Oui, enfin. Plus besoin de répondre 'utilisez juste votre nom d'utilisateur et pas username@disroot.org' pour la centième fois chaque jour :P Grâce à **Meaz**, nous avons enfin lancé et compilé une **FAQ** sur le site \o/ De plus notre bot **milkman** (le nom est sujet à changement, la blague n'est plus drôle), peut maintenant aussi répondre aux questions **FAQ** pour tous ceux utilisant le chat. **Antilopa** continuera à travailler sur les améliorations visuelles et esthétiques de la page et bien sûr nous ajouterons plus de questions et encore plus de réponses au fur et à mesure du temps.

# Pilule rouge

**RedPill** est notre version du chat en ligne **XMPP** de **Opcode** appelé **ConverseJS**. Ces dernières semaines **Muppeth** a décidé, en tant que projet parallèle, de travailler sur l'aspect du webchat. Résultat que vous pouvez observer à [https://webchat.disroot.org](https://webchat.disroot.org). Ce n'est que le début de ce changement tant attendu. Dans un avenir proche, nous serons occupés à mettre en œuvre diverses fonctions telles que l'audio/vidéo conférence, les messages vocaux, les réactions, ainsi que de nombreuses améliorations visuelles. Ce n'est qu'un aperçu. Le nom pour notre version du client était une idée de dernière minute, donc la couleur principale et dominante est le bleu au lieu du rouge mais nous allons voir ce que va faire le rouge dans ce contexte. Pour ceux qui veulent suivre le travail sur le webchat, vous pouvez utiliser [https://devchat.disroot.org](https://devchat.disroot.org) où nous jouons et testons des idées.

# Module Diaspora sur Hubzilla

Malgré les innombrables heures passées à déboguer des problèmes sur notre connecteur **Hubzilla** vers le réseau **Diaspora**, nous n'avons fait aucun progrès en termes de trouvaille d'une solution. Actuellement (comme c'est le cas depuis des mois en fait) le module **Diaspora** est désactivé. Pour l'instant, nous avons décidé de ne pas consacrer plus de ressources à la recherche de solutions. Nous continuerons cependant à rendre compte aux principaux développeurs de **Hubzilla**. Nous espérons que bientôt, un bug sera repéré (si ce n'est pas par nous alors peut-être quelqu'un d'autre) et nous allons maintenant nous concentrer plutôt sur le fait de sortir **Hubzilla** de notre version bêta et lui faire sa place sur le site comme le réseau social sur lequel se rendre. Cela nous amène à une fin plutôt triste de l'article...

<br><br><br>
Comme vous pouvez le voir, il y avait pas mal de choses. Ne vous inquiétez pas, les prochains messages seront beaucoup plus courts car nous sommes maintenant très stricts sur la quantité de travail que nous assumons :P
