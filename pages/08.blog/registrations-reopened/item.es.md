---
title: '¡Registros abiertos de nuevo!'
date: 10/26/2018
media_order: poland-communist-reality.jpg
taxonomy:
  category: news
  tag: [disroot, noticias, registro]
body_classes: 'single single-post'

---


Como mencionamos en la publicación anterior, las últimas dos semanas y algo más estuvimos ocupados trabajando para lograr tener los registros de vuelta y funcionando. Esperamos que el proceso no sea demasiado complejo y que podamos lidiar con la carga de trabajo extra que implica la aprobación manual. Estuvimos escribiendo un puñado de herramientas que deberían ayudarnos a mantener el proceso tan sencillo y rápido como sea posible. Estaremos revisando las solicitudes de nuevos usuarios una vez al día, así que prepárense a esperar unas horas cuando registren un usuario nuevo.
Esperemos todos que este proceso funcione bien y que no tengamos que repensar de nuevo toda la manera de abordarlo. El tiempo dirá.

Proceso automatizado de eliminación de cuentas.
Mientras planeábamos y trabajábamos en el nuevo sistema de registro de usuarios, pensamos en una manera de convertir el actual sistema de eliminación en uno completamente automatizado. En este momento, este sistema es bastante manual, es decir, quitamos las cuentas y los datos semanalmente. Esto no es lo ideal. Los usuarios tienen que llenar el formulario, contestar un correo, etc., y como tenemos que realizar el proceso a mano, a veces (habitualmente) el proceso se demora a medida que cosas más importante van surgiendo. De ahora en adelante, finalmente podrás dar de baja tu cuenta a través de [https://user.disroot.org](https://user.disroot.org). Yu cuenta será deshabilitada inmediatamente y los datos eliminados automáticamente en 48 hs. Ya puedes utilizar el sistema, aunque la auto-eliminación de los datos estará implementada en los próximos días ya que aún no lo hemos terminado.

**¿Qué tenemos planeado para las próximas dos semanas?**
Las próximas semanas queremos enfocarnos en las actualizaciones de algunos servicios que requieren más trabajo personalizado que lo habitual:

Hubzilla acaba de tener una actualización grande, cambiando el sistema más bien complejo de configuraciones a uno más sencillo de utilizar. Esto significa que necesitamos probarlo y asegurarnos que nuestras opciones predefinidas para los nuevos usuarios permanezcan intactas.

Taiga y Discourse necesitan bastante trabajo ya que las actualizaciones están bloqueadas por cambios manuales personalizados que tenemos que aplicar para que funcionen. Algo que queremos automatizar desde hace tiempo y que ya no podemos seguir demorando.

Nextcloud necesita un montón de amor. Durante los últimos hemos observado un salto pronunciado en su uso, lo que significa también la necesidad de cantidades de recursos. Además de actualizar Nextcloud a la versión 14, queremos empezar a trabajar en algunas mejoras, optimizaciones y cambios que permitirán una mejor performance y, más importante, un impacto menor.

¡Saludos!
