---

title: "Este año no podría ser más loco"
date: '16-03-2020 20:00'
media_order: Italians-sing.jpg
taxonomy:
category: noticias
tag: [disroot, noticias, privacidad, hubzilla, correo]
body_classes: 'single single-post'

---
Ya han quedado atrás un par de sprints del 2020. Hay muchas cosas nuevas en las que hemos estado trabajando y también pasando por un poco de caos. Por lo tanto, decidimos cambiar la duración de nuestro trabajo planificado a corto plazo (el sprint) de 2 a 4 semanas. Esto nos da un poco más de espacio para respirar y bajar un poco el ritmo. Además, hemos hecho algunas mejoras administrativas adicionales en la forma en que dividimos y supervisamos cada unx el trabajo de lxs otrxs, y de darnos feedback. Esperamos de esta manera reducir el caos y mejorar la comodidad en el trabajo que tuvimos en las últimas semanas.

Ok, pasemos de inmediato a lo que hay de nuevo.

## Versiones 1.2 de Declaración de Privacidad y Términos del Servicio

Desde hace un tiempo ya venimos trabajando en la nueva versión de nuestra **Declaración de Privacidad**. **Fede** asumió un rol protagónico juntando todas las ideas e información faltante y gracias a él tenemos una nueva versión de los documentos. No hay grandes cambios en la esencia del texto, aunque sí cambios notorios en la organización, estructura y claridad del contenido. Nos hemos centrado en hacerlo lo más claro y detallado posible para que sea fácil de leer y entender. Y, por supuesto, también para cumplir con las directrices de la **RGPD**. Añadimos información detallada sobre los aspectos específicos de cada servicio provisto por **Disroot.org**. Esperamos que aprecien el trabajo y que las cosas sean más claras. Además, para seguir mejor los cambios en la **Declaración de Privacidad** hemos creado un repositorio git (sistema de control de versiones) [aquí](https://git.disroot.org/Disroot/Disroot-Privacy-Policy/commits/branch/master) donde pueden seguir todas las modificaciones en el documento.

Es de esperar que también ayudará a todxs lxs usuarixs de los servicios en línea a ser más responsables con sus datos -independientemente del continente en el que vivan, de la normativa vigente, de los proveedores que utilicen y de las necesidades que tengan- y a saber cómo defenderlos.

Los cambios en los **Términos del servicio** también son cosméticos, además de la nueva adición del requisito de la edad mínima que hemos establecido en 16 años. De manera similar a la **Declaración de Privacidad**, los cambios en el documento pueden ser seguidos [aquí](https://git.disroot.org/Disroot/Disroot-TOS/commits/branch.master).

## Retención de datos almacenados de Etherpad y XMPP

Otros dos cambios notables en la nueva Declaración de Privacidad han sido las modificaciones en el período de retención para los datos almacenados tanto en **Etherpad** como el **chat (XMPP)**.

Para Etherpad (igual que para Etheralc) los pads que hayan estado sin tocar por seis meses serán borrados del servidor.

El período de almacenamiento del historial de chat XMPP también fue cambiado de seis meses a uno. Después de consultar con otrxs Administradorxs, desarrolladorxs de servidores XMPP y usuarixs, llegamos a la conclusión de que un valor de seis meses era desmedido. Ya que el historial se almacena en nuestros dispositivos, el servidor debería guardar solo una cantidad mínima y razonable y pensamos que un mes es suficiente.

Estos cambios serán efectivos a partir del 1ro de abril. Esto les da tres semanas para prepararse si la necesitan.

## Trabajos de preparación

Pasamos enero y febrero preparando el terreno para todos los trabajos futuros de nuestros temas principales: **Mejoras en el correo electrónico y lanzamiento del Hubzilla**. Todo esto nos permitirá trabajar adecuadamente en cosas como el cifrado de los buzones de correo, una mejor protección contra el spam, un nuevo correo web, así como un montón de mejoras en nuestra instancia de Hubzilla (DisHub) y nuestra aplicación de Hubzilla para Android llamada **Nomad**. Por aburrido que parezca, este trabajo fue esencial para las mejoras que vendrán.

## Actualización de Nextcloud 18

Sip. Finalmente ha llegado. El último fin de semana hemos implementado la actualización que muchxs de ustedes esperaban. Breve descargo de responsabilidad: La solución OnlyOffice no parece estar soportada todavía por el cifrado del lado del servidor que usamos, sin embargo seguimos investigando para confirmarlo con seguridad.

Aquí un vistazo a algunas de las nuevas características disponibles:

- **Aplicación Archivo** - El encabezado de la carperta admite agregar información adicional en la forma de un archivo "léeme" que se muestra en la parte superior de la carpeta. Ahora puedes ver la lista de usuarixs con lxs que compartes el archivo.
- **Aplicación Foto** - La Galería ha sido reemplazada por la aplicación Foto. Luce renovada y permite compartir fotos más fácilmente
- **Aplicación Calendario** - Ahora puedes crear una sala para Hablar directamente desde la aplicación Calendario y así discutir colaborativamente un evento.
- **Cambiar de Propietarix** - Permite transferir fácilmente la propiedad de un archivo o una carpeta a otrx usuarix.
- **Video verificación de archivos protegidos con contraseña** - Permite la video verificación de tus archivos compartidos con contraseña para mayor seguridad

## El Fenomenal Desafío de Fin de Año 2019 del Alias Adicional

En diciembre de 2019, ¡el objetivo de nuestra campaña de fondos fue alcanzado! Recaudamos 1500€. ¡Queremos agradecer una vez más a todxs lxs que decidieron donar a Disroot durante todo el año! Estamos muy agradecidos.

Desde ahora, cada Disrooter tiene un nuevo alias: *tunombredeusuarix@getgoogleoff.me*

Si no sabes cómo configurarlo, revisa nuestro [tutorial](https://howto.disroot.org/es/tutorials/email/alias)

## Probando Roundcube

Como algunxs de ustedes saben, estamos planeando reemplazar el actual correo web Rainloop con Roundcube. La razón de ello es que pensamos que Roundcube puede integrarse mejor con lo que planeamos hacer en un futuro próximo (WKD, GPG) pero también parece ser una solución de correo electrónico mucho más pulida. Además queremos lograr una mejor integración de Nextcloud y XMPP en el webmail y por eso decidimos que Roundcube es la plataforma con la que podríamos hacerlo. Para aquellxs que quieran ver un adelanto y ayudarnos con comentarios, sugerencias, y a dar forma al nuevo webmail, ya pueden usarlo en http://roundcube.disroot.org. Solo tengan en cuenta que esta es una instancia de prueba, lo que significa que las cosas pueden romperse o que podemos borrar su configuración (aunque no sus correos).

## Próximamente

Este mes continuamos el trabajo de preparación. Estamos trabajando en las mejoras de la vinculación de dominios personalizados que, como algunxs de ustedes experimentaron, es como una tediosa tarea sin fin.

Para tener una mejor visión general para nosotrxs y tal vez para la comunidad de Hubzilla en general, queremos documentar nuestras ideas para mejorarla visualmente. Esto nos permitirá, por un lado, establecer un objetivo sobre cómo queremos que nuestra instancia Hubzilla se vea y se sienta (no estamos hablando solo del aspecto sino de la experiencia de usuarix en general), pero también, esperamos, ayudará a los desarrolladores de Hubzilla a mejorar esta increíble pieza de software.

Otra cosa que planeamos impulsar este mes es nuestro Informe Anual que, como notaron, está un poco atrasado dado que ya estamos en marzo. Pero, hey, han sido un final y un comienzo de año muy ocupados.

**Todxs, manténganse a salvo, cuídense y a sus allegados. Quédense en casa si pueden y superemos este momento extraño y surrealista rápido y sin demasiadas secuelas totalitarias y distópicas que afrontar más tarde**
