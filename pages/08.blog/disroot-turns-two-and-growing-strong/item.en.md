---
title: 'Disroot turns two and growing strong'
date: 08/09/2017
taxonomy:
  category: news
  tag: [disroot, news, openmailbox, birthday]
body_classes: 'single single-post'

---

**Hi disrooters,**
Last weekend due to openmailbox-gate we had a flood of new comers looking into using Disroot.
Last week (4th aug.) was also disroot’s 2nd anniversary \o/ – a perfect time mark to welcome such influx of new users, we hope you will have much fun using our services!

However, as much as we are delighted by the course of events, Our servers are somewhat struggling. For last nights we’ve been working on optimizing and tuning our setup to cope with all the new visitors and extra load. We are getting there, but there is still some tweaking needed so please forgive us for any performance issues on the cloud. We hope things will stabilize in a few days.

We are also getting lot of questions regarding the project so we would like to answer some:

1. At the moment we are not a legal entity. We are about to become one. Within the next few weeks (as older disrooters know) we will become non-profit foundation.

2. Email aliases is work in progress

3. Privacy policy is work in progress – in short, we don‘t track, we don‘t use analytics (no google nor piwik and such), we do not collect data other then what is necessary for services to function (we store your emails on our server, the files you upload, we know your username etc),

4. More information such us our costs, status, future plans, some statistics will be posted soon (within two weeks) on our annual report that we would like to share in a straight forward transparent way.

5. We are working on a new website which means we will update a lot of information. Ahhh…. and no more JavaScript requirements on the website.

6. “I’m getting HTTP 500 Error when creating account” – Seems like this error appears when using chrome browser. We are looking into it, but in the mean time, you can ignore it as your account was aready created, regardless the error / or use firefox

7. For Updates on downtime, scheduled maintenance work or issues with any of the services please check https://state.disroot.org or #state:disroot.org (matrix room)

8. For News, announcements and changes you can visit our website, follow us on diaspora or mastodon.


Just a reminder to all new comers. **Disroot is not a company!** we give no warranty for our services, nor for the data, although we are doing our best to provide the best possible experience which also includes daily backups (stored for 7 days). As much as we are trying to answer all questions and help everyone, we also have to keep our day–jobs, take care of our family and other projects we are involved with, so please be patient with us.

--------------------

*If you feel Disroot is so awesome it should replace our day-jobs (or just get better servers so more people can use it), you can send your money using any of the methods specified at https://disroot.org/donate – if every user would buy us a “beer” or two each month it would be enough to keep this project running without a stress (we promise we won’t really spend it on beer).*

![](cake.jpg)
