---
title: "DisNews #3 - Progreso de Lacre; vinculación de dominio personalizado; nuevo tablero; la renuncia de Muppeth; 3er cuota por voluntariado"
media_order: news_image.jpg
published: true
date: '20-11-2021 14:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - lacre
        - custom_domain
        - volunteer
        - dashboard
body_classes: 'single single-post'
---

Hola, Disrooters.

*Hace tiempo que no tienen noticias nuestras. De hecho, este post fue redactado en agosto, pero como siempre (nos damos cuenta de que esto ocurre con demasiada frecuencia últimamente), la vida se interpone y las cosas se retrasaron un poco antes de que nos diéramos cuenta que quizás debíamos enviarlo. Así que imaginen que lo están leyendo en algún momento de agosto.*

Estamos orgullosos de hacerles saber que hemos estado ofreciendo nuestra colección de servicios durante 6 años ya. El 4 de agosto, fue el cumpleaños de **Disroot**. Estamos muy felices con lo que hemos podido hacer hasta ahora y esperamos ansiosos que lleguen otros 6 años más. Así que la próxima vez que estén tomando algo, beban también por **Disroot**.

Habiendo comenzado estas Disnews con semejante tono de celebración, veamos qué hay de nuevo ya que ha pasado bastante tiempo desde nuestra última publicación.

# Entorno de pruebas para Lacre

Aunque todavía a ritmo lento, hemos logrado avanzar con los trabajos en **Lacre**, nuestro proyecto de cifrado del buzón. Conseguimos configurar un entorno de pruebas adecuado que nos permitirá romper y reconstruir fácilmente la configuración. Hemos hecho algunas pruebas iniciales y estamos listos para impulsar las cosas. **pfm** ha trabajado en el código, notado algunos problemas y está listo para actualizar el código base a Python3. El curso de acción actual es el siguiente: Primero, queremos introducir pruebas de código. Luego actualizaremos el código base en el [backend](https://es.wikipedia.org/wiki/Front_end_y_back_end) a Python3 y el [frontend](https://es.wikipedia.org/wiki/Front_end_y_back_end) a Php7/8 sin cambiar ninguna característica. Una vez hecho esto, queremos subir los cambios. Eso será un punto de partida para que podamos navegar el barco de **Lacre** en un rumbo ligeramente diferente al del proyecto [upstream](https://es.wikipedia.org/wiki/Upstream_(desarrollo_de_software)). Por supuesto, vamos a subir los cambios, sin embargo, a partir de ese momento algunas de las características nuevas o eliminadas podrían no ser aceptadas por el upstream (por ejemplo, la eliminación de la función de descifrado), lo que significa que probablemente **Lacre** comience a vivir su propia vida. Estamos deseando que lleguen las próximas semanas. Esto, aunque en su infancia, es un momento muy emocionante para el cifrado de buzones de código abierto. Para facilitar el desarrollo de nuestra infraestructura de prueba, hemos respaldado el rol inicial de Ansible para el backend de [**Lacre**](https://git.disroot.org/Disroot-Ansible/gpg-lacre) y subido el rol general del [servidor de correo](https://git.disroot.org/Disroot-Ansible/mailserver).

# Vinculación de dominio personalizado

¡Ah, sí! Después de mucho tiempo, la función de **dominio personalizado** ha vuelto. Hemos decidido cambiar un poco el juego. Esta funcionalidad sigue siendo una "recompensa" por donar. Sin embargo, a diferencia de lo que ocurría antes, esta vez la vinculación se hace a partir de una sola donación y es una función de por vida. ¡Más beneficioso! Además, estamos vinculando el dominio no solo al correo electrónico, sino también al chat XMPP (aunque esta última característica es todavía experimental). Lo que significa que también puedes chatear con otrxs usando tu propio nombre de dominio. Si es solicitado, también se pueden crear salas de chat bajo tu propio dominio.

Aunque hemos decidido que ya no tienen que hacerlo de forma regular, les pedimos que consideren donar al menos el equivalente a 12 cafés de sus cafeterías favoritas al año. *Por favor, recuerden también que las donaciones frecuentes nos permiten mantener el proyecto a flote, pagar las facturas de los servidores y el hardware y, eventualmente, poner comida en nuestras mesas*.

Pueden solicitar que un dominio sea vinculado con nuestros servicios de correo y chat XMPP a través de este [**enlace**](https://disroot.org/en/forms/domain-linking-form).


# Nuevo tablero y tema de Searx

Decidimos combinar tanto el [**tablero**](https://apps.disroot.org) como nuestra instancia [**Searx**](https://search.disroot.org) en uno. Ahora, en lugar de un tablero de mandos básico con muy poca funcionalidad, tenemos una página de aterrizaje completa que se puede utilizar como acceso directo a todos los demás servicios de **Disroot** y como lugar para buscar en la web. Estamos muy contentos con el aspecto unificado que estamos poco a poco implementando en todos los servicios. Puedes elegir entre **beetroot** (tema claro) y **darkbeet** (tema oscuro). En el futuro introduciremos más funciones en la página principal, como alertas sobre problemas o mantenimientos programados. Estamos deseando mejorar la página principal y estamos a la expectativa de sus comentarios. Esperamos que les guste el cambio, a nosotros seguro que sí.


# Muppeth renuncia

Finalmente podemos utilizar este tema como [clickbait](https://es.wikipedia.org/wiki/Clickbait). **Muppeth ha decidido renunciar...**

...a su trabajo.

> Después de casi seis años de vivir prácticamente dos vidas sin descanso y muy pocas horas de sueño, he decidido que no puedo seguir manteniendo esto sin perder la salud y la cordura. Dejar **Disroot** no era una opción ya que es el proyecto más importante de mi vida, así que tuve que tomar la decisión y elegir por una opción más incierta, arriesgada pero seguramente emocionante. Decidí enfocar mis esfuerzos en **Disroot** y trataré de convertirlo (de una forma u otra) en un proyecto que ponga comida en la mesa de mi familia. Creo que proyectos como el nuestro, en los que se requiere tu presencia activa las 24 horas del día, igual que cualquier plataforma comercial, deberían ser capaces de ganar suficiente dinero de forma ética, tratando a sus usuarixs, sus datos y privacidad con respeto. Creo que mejores plataformas, más sostenibles y éticas no deberían ser excluidas de poder ganarse la vida sin sacrificar sus ideas, y creo que esto se puede lograr con **Disroot** y su vibrante comunidad. Quién más si no nosotros.

Estamos muy felices por **Muppeth**, que ha tomado esta difícil decisión, y apoyamos sus esfuerzos y los de todo el equipo por encontrar una forma de hacer que **Disroot** sea financieramente sostenible. Realmente no hace falta tanto para hacer realidad ese sueño. Si cada Disrooter decidiera donar el equivalente a un solo café al mes en apoyo de la plataforma que usan diariamente, podríamos actualizar el hardware sobre el que funciona **Disroot**, donar a proyectos **FLOSS** (Software libre y Código Abierto) para que siga avanzando el software que todxs usamos y del que nos beneficiamos diariamente; y pagar a todo el equipo un salario digno. No son necesarios capitales de riesgo, inversiones turbias, características con fines de lucro o la minería de datos.

Así que, no sean tímidxs y [**donen**](https://disroot.org/es/donate)


# 3ra cuota por voluntariado

Para demostrar que el punto anterior está al alcance, nos gustaría anunciar con orgullo que **ahora podemos pagar una cuota por voluntariado a un miembro más del equipo principal**. Las cuotas por voluntariado, de acuerdo con la legislación holandesa, pueden ser pagadas por las fundaciones sin fines de lucro si no superan los 1.700€ al año. Decidimos pagar una cuota por voluntariado de 140€ al mes si se cumplen ciertas condiciones:

 - Después de cubrir todos los costos, donar a proyectos FLOSS y reservar 400€, necesitamos tener más de 140€ durante un periodo de, al menos tres meses, antes de empezar a pagarlo *(red de seguridad)*.
 - Los 140€ se pagan a un miembro del equipo principal una vez que se alcanza un margen de 140€ durante tres meses consecutivos *(cinturón de seguridad)*.

Nos gustaría agradecer a todxs aquellxs de ustedes que deciden contribuir económicamente con **Disroot**. Estamos muy agradecidos por tener semejante comunidad de Disrooters alrededor del proyecto ❤️


# El futuro de Framadate y Ethercalc

El software libre y de código abierto está en constante evolución y nosotros, como parte de ese movimiento, nos vemos afectados por él. El software en general evoluciona, sigue cambiando y mejorando, pero también a veces deja de progresar o incluso se extingue. Estamos ante uno de los últimos ejemplos. Durante bastante tiempo **"EtherCalc"**, el software que hace funcionar nuestro servicio en [**https://calc.disroot.org**](https://calc.disroot.org) no ha estado bien mantenido, si es que lo ha estado. Recientemente, **Framadate**, que impulsa [**http://poll.disroot.org**](https://poll.disroot.org), se ha unido a ese estado. Viendo que ambos proyectos están en terapia intensiva, empezamos a preguntarnos si tiene sentido seguir brindándolos. Esto significa que ni **Framadate** ni **Ethercalc** recibirán nuevas funcionalidades y con el tiempo pueden surgir problemas de seguridad que no se resuelvan.

Además, una pieza de software que realmente hemos llegado a amar, **CryptPad**, una completa suite de colaboración de conocimiento cero cifrada de extremo-a-extremo que motoriza nuestra instancia [**https://cryptpad.disroot.org**](https://cryptpad.disroot.org), incluye funcionalidades de encuestas y hojas de cálculo. **CryptPad** está desarrollándose activamente y proporciona algo que, en vista de los recientes cambios en la legislación, no solo en la UE, sino en todo el mundo, es cada vez más importante, el cifrado de extremo-a-extremo. Un tipo de cifrado en el que los datos se almacenan en los servidores de forma que ni siquiera los administradores con acceso total al hardware puedan descifrarlos. Sabemos que ambos servicios han estado con nosotros desde el inicio de **Disroot** y han sido utilizados por muchos Disrooters. Nos gustaría conocer su opinión sobre este tema y saber lo que piensan. Pueden utilizar cualquiera de las vias de contacto que aparece en nuestro [**sitio**](https://disroot.org/contact). Además, quisimos crear una sencilla encuesta para poder tener algunas *estimaciones* sobre sus opiniones generales al respecto. Por favor, consulten el siguiente enlace:

[**Encuesta Framadate/EtherCalc vs CryptPad**](https://cryptpad.disroot.org/form/#/2/form/view/t+aoDjPYZRjdT-wZQer3TdKJn8X3NKJhbDQHvc+yNKg/)


Una vez más, ¡gracias por navegar con nosotros durante los últimos 6 años! Ha sido un placer para nosotros apoyarlos con los servicios y ha sido una aventura increíble hasta ahora. ¡Todavía no hemos dicho la última palabra y vemos que el viaje recién empieza!

Gracias por estar con nosotros, por su apoyo, comentarios, amistad, lágrimas y amor. ¡Gracias por considerar usar esa pequeña isla en el mar de internet y por quedarse por aquí! ¡Nos estamos viendo!
