---
title: 'Cese del servicio de RSS'
media_order: paperpile.jpg
published: true
date: '05-04-2021 17:00'
taxonomy:
    category:
        - novedades
    tag:
        - disroot
        - nextcloud
        - news
        - rss
body_classes: 'single single-post'
---
Bien, esto probablemente vaya a molestar a algunxs de ustedes, pero esperamos que nuestro argumento sea convincente.

Hemos decidido dejar de ofrecer **Nextcloud News** (el lector de RSS) o cualquier otro lector online de RSS.<br>
La razón tiene que ver puramente con los recursos de hardware necesarios para mantenerlo y el hecho de que el servicio está siendo muy abusado y mal utilizado por una gran mayoría de lxs usuarixs.

Desde hace rato estamos peleando con la misma situación: descargamos un montón de artículos de noticias (alrededor de 1 millón a la semana), de los cuales entre 10 y 20 mil de ellos están configurados para marcarse como leídos después de tres semanas. Esto significa que la mayoría de los artículos que descargamos y almacenamos en el servidor nunca son leídos. Esto sucede porque es muy fácil seguir agregando más sitios para leer pero en realidad no hacerlo jamás (_los máximos usuarios tienen varios miles de sitios de los cuales se descargan artículos_).

La situación se volvió tan mala que el espacio en la base de datos solo para los artículos de noticias excede dos veces la cantidad necesaria para almacenar toda la caché de archivos, calendarios, contactos, tableros de proyecto (lo que se les ocurra) que están actualmente almacenados en el servidor. Contactamos a los desarrolladores pero no estaban interesados en encontrar una solución porque simplemente no tenían intención de que la aplicación funcione a tal escala. Por consiguiente, buscamos alternativas y consideramos implementar el servicio de **TTRSS** como reemplazo. Este servicio ofrece mejores configuraciones de administración con las que podríamos fácilmente purgar todos los artículos antiguos sin leer. Sin embargo, mientras trabajábamos en la implementación, hablamos con algunas personas que ya lo proveen y descubrimos que, a la larga, la situación podría no ser mucho mejor ya que los problemas de desempeño y el espacio de la base de datos tienen también aquí un impacto alto.

Esto nos lleva a una conclusión. Como usuarias y usuarios nosotros mismos del RSS, nos dimos cuenta que realmente no necesitamos servicios online que hagan las cosas por nosotros. Todos los [lectores de RSS](https://alternativeto.net/browse/search/?q=feed%20reader&license=opensource), disponibles para todas las plataformas y sistemas operativos, brindan alguna opción para usar RSS localmente. Esto quiere decir que tu aplicación/cliente haría el trabajo que hace el servicio online pero directamente en el dispositivo (guardar lista de feeds, descargar artículos nuevos, permitir la lectura, etc). Viendo cuán propensas al desperdicio son las soluciones en línea, no vemos razón para que haya una de ellas haciendo de intermediaria. Con los archivos OPLM, que puedes exportar/importar fácilmente, puedes utilizar la misma lista de feeds en distintos dispositivos, o tener la posibilidad de guardarla como respaldo. La única ventaja de tener un solución RSS online es el hecho de que puedes esconder tu dirección IP de los sitios de los cuales descargas artículos (aunque si descargas imágenes u otros elementos la IP de tu cliente será registrada de todas maneras), pero pensamos que esta no es una razón suficientemente buena para generar semejante carga.

Estas son las razones por las que hemos decidido dar de baja el servicio y promover el uso de clientes de manera local (como lo hacemos nosotros). Esto es lo que va a ir sucediendo para darles tiempo suficiente a ustedes para migrar a otro servicio en línea o pasarse al modo local:

  - A partir de hoy, todos los items de los feeds (artículos) serán limpiados de nuestra base de datos
  - Por defecto, la aplicación Noticias estará deshabilitada para nuevxs usuarixs
  - No descargaremos nuevos artículos
  - ⚠️ **Interrumpiremos la aplicación Noticias completamente el 31 de Mayo de 2021**, así que tienen suficiente tiempo para exportar sus archivo OPLM antes que eso suceda.

<br>
Puedes ver cómo exportar tus feeds [aquí](https://howto.disroot.org/tutorials/user/gdpr/nextcloud/news)
