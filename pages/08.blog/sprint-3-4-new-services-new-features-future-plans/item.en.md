---
title: '[Sprint 3-4] – New services, New features, Future plans'
date: 11/15/2015
taxonomy:
  category: news
  tag: [disroot, news, searx, lufi, framadate, taiga]
body_classes: 'single single-post'

---

**Dear disrooters,**

Last weeks we were busy adding new services and features to the disroot platform. At this moment we have all the services we were planning to host (at least for now) set up and ready to use. We have one more project in mind (loomio.org) but we’ll consider it more seriously somewhere next year if we still have enough resources (server space) to host it. For now we still have a lot planned for the upcoming two months.

## So what in our road map for the near future?

We would like to focus on improving disroot’s features so it can become a better platform for groups, collectives, non-profit organizations and communities. We want to create a a set of tools for group members (admins) to easily add/remove users, user groups domain-names etc.
We want to introduce single sign on service where users would be logged in to all services at once.
We are also planning a lot of user interface integration, so you could use all the disroot goodies from one place.
Last but not least. We want to create as many tutorials and howtos as we can to make it easier for all of you to use and integrate our services with your computers, laptops and mobile devices. If you like writing things, you might consider giving us a helping hand. [Forum](https://forum.disroot.org) is at your disposal.

We hope to be ready to launch disroot for groups in the beginning of 2016. Stay tuned.

## What’s new in disroot?

New features:
1. **Forum** – Users can now use disroot account to register and login to the forum.
2. **Webmail** – Webmail is now integrated with cloud (owncloud).
3. **Cloud** – New version of owncloud brings some new features such as: improved user interface and new gallery application.
4. **Etherpad** – has been updated with a set of nice new features and plugins.

## New Services:

[search.disroot.org](https://search.disroot.org) – powered by the Searx project, a metadata anonymous search engine. It searches your queries using all kind of search engines (Google, Yahoo, Bing, TPB, Duckduckgo etc) and shows you all the results in one window. Furthermore, it prevents those search engines from creating user profiles based on your searches. You can add our search to your browser by following link. Read more…

[upload.disroot.org](https://upload.disroot.org) – an encrypted file upload and share service powered by Lufi. You can now upload files for maximum time of 60 days and share it with others using a link. No registration is required. All files are encrypted before they land on our servers to provide perfect balance between privacy and comfort. Read more…

[poll.disroot.org](https://poll.disroot.org) – Online service for planning an appointment or making a decision quickly and easily. No registration is required! This service is powered by Framadate.

**Note!** We are still testing it. There are some issues we know of and we are trying to resolve them as soon as possible. However we thought you can give it a try already. What doesn’t work then? You cannot modify your choices. Check the progress on the matter here

[board.disroot.org](https://board.disroot.org) – Project management board for your group! With this tool you can get an overview of your current projects state, set tasks to accomplish and assign them to your team members. Planning has never been that fun! [Read more…](https://disroot.org/services/board)

We are very happy with the recent achievements and we hope you will enjoy them too.

##How to get involved with disroot?

  - You can follow development [here](https://board.disroot.org/project/disroot-disroot/backlog), where you can also vote for features you find most important (login required).
  - Or join discussions on [forum](https://disroot.org/c/disroot)
  - Submit bugs and suggest features [here](https://board.disroot.org/project/disroot-disroot/issues?page=1)
  - Send us an email: support@disroot.org
