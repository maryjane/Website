---
title: 'Cierre de Matrix'
media_order: what_if_xmpp.jpg
published: true
date: '07-09-2018 14:50'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - matrix
body_classes: 'single single-post'
---

Como algunos de ustedes ya habrán oído, hemos decidido discontinuar parcialmente nuestro servidor Matrix. Descubrimos Matrix mientras intentábamos mejorar nuestro servidor XMPP (irónicamente, durante un sprint llamado: *"¡Hacer a XMPP genial de nuevo!"*) y nos dimos cuenta cuán grande era el potencial de esta nueva herramienta de comunicación. Ahora, después de casi dos años estar corriéndola, llegamos a la conclusión de que no le dimos a XMPP una oportunidad justa y por lo tanto nos gustaría de vuelta a crear un servidor XMPP poderoso.

Aunque todavía pensamos que Matrix tiene el potencial para convertirse en una plataforma grandiosa, hay varias razones por las que no la podemos seguir manteniendo en este momento. Los recursos necesarios para ello no pueden ser provistos sin la financiación apropiada pero no es la única razón: mantenimiento constante, resolver dificultades de federación o problemas generales cotidianos están, sencillamente, consumiendo demasiado de nuestro tiempo, energía y estrés. Pero lo que está empujándonos realmente a XMPP es cómo está diseñado con la privacidad del usuario como un pilar central. Mientras el protocolo Matrix está tomando también esa privacidad seriamente, parece que su enfoque estructural está dirigido a otro lugar, valorando facilidad de uso por encima de la privacidad. Matrix almacena la información publicada en cualquier sala indefinidamente en todos los servidores participantes. Las direcciones de los usuarios son visibles para cualquiera que esté participando en la sala (en el caso de las salas públicas, significa, básicamente, para todos). Entendemos que esto no es con intenciones dañinas y es, en este momento, constituyente del protocolo. Pero, con las crecientes presencias de varios bots mapeando la red Matrix entera, y de grupos de extrema derecha, consideramos que es problemático en nuestro caso práctico.

Con XMPP, los IDs de usuarios están ocultos por defecto, a menos que sea habilitado por los administradores de sala. La información puede ser almacenada en el servidor pero también puede ser configurado por los administradores e individualmente por los usuarios para que no lo haga según sea el caso de una sala o contacto. Como Matrix, la mayoría de los clientes/aplicaciones XMPP soportan cifrado de extremo a extremo. Los datos son almacenados solamente en un servidor, en el que la sala vive, lo que tiene sus costados tanto positivo como negativo. Por un lado, podría parecer demasiado centralizado y propenso a fallar, pero por otro, es grandioso saber donde vive tu información.

Sabemos también que XMPP no es perfecto, pero ha estado por aquí desde hace mucho tiempo y sentimos que necesitamos darle una oportunidad y encontrar por nosotros mismos cuáles son sus desventajas en nuestro caso. Con suerte, encontraremos mínimas :)

Sabemos que algunos usuarios que actualmente utilizan Matrix les gustaría seguir haciéndolo sin la necesidad de migrar sus cuentas (de momento, algo imposible) y sus alias de sala. Tampoco queremos cerrar todas las puertas porque podríamos decidir volver a Matrix en el futuro (nunca se sabe).

Por lo tanto, no estamos cerrando Matrix completamente. Esto es lo que planeamos hacer:

    1. Ya no pueden ingresar a Matrix nuevos usuarios
    2. El historial de sala de todas las salas públicas serán purgados hasta los tres últimos meses
    3. Todos aquellos usuarios que no hayan ingresado en los últimos dos meses serán borrados (automáticamente saldrán de las salas y sus accesos a Matrix bloqueados)
    4. El acceso a Matrix será limitado a aquellos usuarios que hayan estado activos los últimos dos meses
    5. moveremos la instancia de Riot de https://chat.disroot.org a https://riot.disroot.org
    6. Seguiremos purgando el historial anterior a tres meses de todas las salas públicas y, cuando la opción de retención de historial pueda ser aplicada a todo el servidor, lo haremos sobre el historial mayor a seis meses para todas las salas
    7. Naturalmente, mantendremos el software actualizado

Con estas acciones pensamos que encontramos una solución que no daña a los usuarios existentes y nos da algo de espacio para respirar y enfocarnos en otros aspectos de la plataforma Disroot. Les agradecemos a todos por su comprensión y apoyo a lo largo del camino.
