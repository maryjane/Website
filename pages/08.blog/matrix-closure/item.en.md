---
title: 'Matrix Closure'
media_order: what_if_xmpp.jpg
published: true
date: '07-09-2018 14:50'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - matrix
body_classes: 'single single-post'
---

As some of you might have heard, we have decided to partially discontinue our Matrix server. We discovered Matrix while trying to improve our XMPP server (ironically during the sprint called: *"To make XMPP great again!"*) and realized how great the potential of this new communication tool was. Now after running it nearly two years, we came to the conclusion that we did not give XMPP a fair chance and therefore we would like to shift our focus back to create a powerful XMPP server.

Though we still believe Matrix has the potential to become a great platform, there are several reasons why we cannot continue maintaining it at the moment. The resources needed for it cannot be provided without proper funding but that is not the only reason: Constant maintenance, resolving federation issues or general everyday problems are simply consuming too much of our time, energy and stress. But what is really drawing us to XMPP is how it is designed with the user's privacy as a central pillar. While the Matrix protocol is also taking the user's privacy seriously, it seems like its architectural focus is directed elsewhere, valuing usability over privacy. Matrix stores the data posted to any room indefinitely on all participating servers. User addresses are visible to anyone participating in the room (in case of public rooms, it basically means to anyone). We understand this is not meant for any malicious intention and is, at the moment, integral to the protocol - but, with the growing presence of various bots mapping the entire Matrix network and the growing presence of the alt-right in the network, we consider that problematic in our use case.

With XMPP, user IDs are hidden by default, unless enabled by room admins. The data can be stored on the server but can also be set to not be stored by admin and individually by users per room/contact case. Just like Matrix most of the XMPP clients/apps support end to end encryption. Data is stored only on one server, where the chat-room lives, which has both positive and negative sides. On the one hand it might seem too centralized and prone to failure, but on the other hand, it is nice to know where your data lives.

We know that XMPP is also not perfect, but it has been around for very long and we feel we need to give it a go and find for ourselves what are its downside in our use case. Hopefully we will find them minimal :)

We know some users who currently using Matrix would like to continue using it without the need to migrate their accounts (atm impossible) and their room aliases. We also don't want to shut all the doors because we might decide to come back to Matrix in the future (you never know).

Therefore we are not shutting down Matrix completely. This is what we plan to do:

    1. New users can no longer login to Matrix
    2. All room history of all public rooms will be purged to last 3 months of history
    3. All users who have not login in last 2 months will be deleted (they will automatically exit the rooms, and their access to Matrix will be blocked)
    4. Access to Matrix will be limited only to users who have been active in last 2 months
    5. We will move Riot instance from https://chat.disroot.org to https://riot.disroot.org
    6. We will keep purging history earlier then 3 month back of all public rooms and, when option of room history retention can be applied server wide, we will apply it on history older then 6 months for all rooms.
    7. Naturally we will keep the software up to date.

With these actions we think we found a solution that does not harm existing users and gives us some breathing space to focus on other aspects of the Disroot platform. We thank you all for your understanding and support all along the way.
