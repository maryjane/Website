---
title: 'Sprint - Solve registrations'
date: 09/27/2018
media_order: Britain_Queues_For_Food_Rationing_and_Food_Shortages_in_Wartime.jpg
taxonomy:
  category: news
  tag: [disroot, news, finances, xmpp, matrix, sprint]
body_classes: 'single single-post'

---


Monday is a mark of yet another sprint. Last two weeks we spent mainly on wrapping our heads around finances. We've moved all of our accounting and financial administration to an awesome looking open source application called [akaunting](https://akaunting.com/). **Antilopa** did a great and very labor intense (and definitely not fun) job of punching in all the numbers from all over the place (paypal, bank account, liberapay, patreon etc) so we can have a clear overview of the current financial state. We are very pleased to see how well we stand financially (hope this trend will stay on course).

We've also managed to do some work on matrix purging. Currently we managed to shrink the database size from 400GB to about 168GB. All this after purging the history on all public rooms older then 4 months. It's a satisfying result and we hope that once we deactivate inactive users, this number will drop even more. This also means we will be running our purging script on regular basis to keep that number low and database clean. ( [script](https://git.fosscommunity.in/disroot/matrix_scripts/synapse_scripts/blob/master/purge_matrix_rooms.sh) if anyone wants to use on their instance).

We have also fixed small issues on xmpp side and we are now scoring 94% on [compliance test board](https://compliance.conversations.im/server/disroot.org/). Hopefully if we scramble few extra hours this coming weeks we will be providing jabber/xmpp service over web port 443 which will mean we are fully compliant.

While doing all this **muppeth** felt artistic and worked a bit on his vision of xmpp-webchat theme for [converse.js](https://conversejs.org) he wants to implement (beware, image below has alarming amount of cats):

![](sketch3.png)



(Its not totally finished and polished, but it gave us a general idea in which direction we want to go with the theme and the base to start implementing).

**Coming two weeks:**
In the upcoming sprint we will focus on the issue that led us to temporarily close the registration. We are planning to do series of brainstorm sessions (between both admins internally, closed group of active disrooters, on the chat and the forum), outcome of which will be hopefully a decision on how to deal with future registrations. We also hope that we will have an idea of how to implement the solution and that it won't take long.
*To everyone who has been asking for account creation:* Please be a bit more patient. We do not want to create accounts on per request basis because of the amounts of them we get on regular basis. If we do not come up with a solid solution in upcoming sprint, or if the implementation will take long time, we will however create user-accounts on per request basis.

Additionally to solving the registration issue, general maintenance, updates, and solving little things here and there, we will implement a new disroot donation page.
For a while now we realized that current donation page was not really illustrating the current state. The planned budget we estimated in august last year was depleted already in January when we invested lots of money in the hardware and that was just the beginning on the year! We recently began to think on ways to better show the situation and give better overview on our monthly income and costs as well as introduce some goals we would like to strive to. We think we came up with a quite nice idea, and we hope you will find it interesting once its online within next two weeks time.

As always, you can follow the development via our [taiga board](https://board.disroot.org/project/disroot-disroot/taskboard/solve-registration?milestone=214)
#disroot #disrootnews
