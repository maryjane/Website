---
title: 'FLISoL 2021'
media_order: flisol_2021.jpg
published: true
date: '17-04-2021 00:30'
taxonomy:
    category:
        - news
    tag:
        - flisol
body_classes: 'single single-post'

---
# FLISoL Online 2021

The **Comunidad Latina de Tecnologías Libres** (Latin American Community of Free Technologies), the **Club de Software Libre** (Free Software Club) and **Libera Tu Radio** (Free Your Radio) organize and invite you to a new online edition of the **FLISoL**, **Festival Latinoamericano de Instalación de Software Libre** (Latin American Free Software Installation Festival), one of the biggest Free/Libre Software installation and promotion events in the world.

The festival starts on **Sunday, April 18, at 6 PM (UTC -3)** and will run until Saturday, April 24.

#### **This year's transmission will be the first one made entirely with Free Software**

The event is focused on all the people interested in knowing more about free/libre software and culture. During the meeting there are talks, lectures and workshops related to Free/Libre Software, in all its range of expressions: artistic, academic, business and social.

More information about the event (talks, schedules, etc) at:

## [**caminoalflisollibre2021.softlibre.com.ar**](https://caminoalflisollibre2021.softlibre.com.ar/)
