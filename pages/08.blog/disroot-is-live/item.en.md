---
title: 'Disroot is live'
date: 04/08/2015
taxonomy:
  category: news
  tag: [disroot, news]
body_classes: 'single single-post'

---

### Create account, test, point out bugs, mistakes, and others


**Dear users, supporters, friends…**

22 days of silence and we are back online. It has been three intensive weeks. Not only because of the amount of work with the new project but also because of great summer weather that was tempting us to spend some of that free time away from the screen. Nights however were long enough to get things done (who needs sleep if you have coffee). We hope you guys like the initial end result and will use our services with pleasure.

### It’s time for testing and feedback.

We are aware there might be some glitches, typos, bugs, errors and other things we’ve missed when setting up. Therefor we count on your feedback. If you find something that isn’t right use our feedback section or write us an email directly to support@disroot.org. We would like to encourage you to use the ‘[feedback](https://forum.disroot.org)‘ section as it will function as a reference for other users that encounter similar problems / questions / issues.

Apart from testing and improving current setup we are staring to work on tutorials and howtos to help you integrate all the services with your devices, and understand and use encryption.
