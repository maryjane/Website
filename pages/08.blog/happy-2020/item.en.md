---
title: 'Happy 2020'
media_order: aliasPinguine.jpg
published: true
date: '01-01-2020 19:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - fundraiser
        - alias
body_classes: 'single single-post'
---

Greetings from the Disroot Team to all Disrooters out there. We hope you had tons of fun last night and are looking forward to 2020 just as much as we do. Last year has been crazy busy and looks like the coming 2020 will be no different. We cannot wait and hope you too have some awesome plans for the coming year.

#DisrootAliasChallenge goal reached !

We are starting the year with an amazing announcement. The goal for our fundraising campain of last week has been reached! We've raised 1500€ one day before the end date. We would like to thank everyone who decided to donate to Disroot not only this last week but throughout the entire year! We are very grateful. You keep us alive ♥

This means @getgoogleoff.me will be added as an alias email address for all Disrooters very soon.

If you donated over 30€ by bank or cryptocurrency, please contact us with your details so we can send your reward your way.

And in case you missed the [video...](https://peertube.co.uk/videos/watch/5a3cbc03-1831-4dee-b2cd-92d80dd51f2b)

## What's next?

Currently, we are closing up the year, finishing last things on our roadmap. We are also working on crystallizing the plans for the new year as well as writing the 2019 report which should be published later this month.

### Cheers, and thanks for hanging out with us. May the new year be amazing for all of you!
