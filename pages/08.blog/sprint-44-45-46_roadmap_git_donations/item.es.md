---
title: 'Hoja de ruta, nueva oleada de donaciones FLOSS, git y nueva página principal'
date: '07-11-2019 16:00'
media_order: roadmap.jpg
taxonomy:
  category: news
  tag: [disroot, noticias, git, servicios, donaciones, floss, 'hoja de ruta']
body_classes: 'single single-post'

---
*(Sprint 44-45-46)*
<br><br>
¡Este post está atrasado dos meses!. De alguna manera, aún cuando teníamos el borrador, lo seguimos empujando hacia abajo en la lista de prioridades hasta el abismo de las tareas olvidadas. Es tiempo de desenterrarlo y publicar una porción de las cosas en las que estuvimos trabajando últimamente.

## Hoja de ruta T4 2019

Estamos trabajando en la mejora de nuestra estructura organizativa interna. Por eso hemos decidido crear una hoja de ruta para el último trimestre de este año. Esperamos que esto nos permita planificar mejor nuestro trabajo y tener una visión y objetivos claros para el futuro. Nuestra intención es continuar con esta forma de planificación en 2020 y más allá. La idea es tener dos tipos de hojas de ruta:

- **Trimestral**: donde nos fijamos metas a corto plazo, nos enfocamos en proyectos específicos.
- **Semestral**: para no perder de vista el panorama general, queremos establecer metas a largo plazo dos veces al año para organizar mejor nuestro trabajo, metas y visión del proyecto.
<br><br>


Notamos que a menudo tendemos a desviar nuestra atención o descuidar algunas partes del proyecto como consecuencia de los recursos limitados y falta de una planificación/visión clara. Las hojas de ruta nos ayudarán a mantener nuestra atención hacia un objetivo común. Cuán bien lo haremos, el tiempo lo dirá.
<br><br>
**Así que, ¿en qué nos vamos a enfocar el resto del año?**
<br><br>
Entre Septiembre y Diciembre, queremos finalizar el trabajo organizativo y estructural en el que ya empezamos a trabajar: la (re)estructuración de la organización interna, la forma en que trabajamos, interactuamos, tomamos decisiones, compartimos responsabilidades. Esto significa pensar en las mejores soluciones, documentarlas, mejorar nuestros flujos de trabajo y procedimientos, así como elaborar una administración más eficaz. Además, queremos centrarnos en pequeñas mejoras en la UX (experiencia de usuarix) y la UI (interfaz de usuarix), especialmente durante el proceso de incorporación. Operar **Disroot** desde hace bastante tiempo nos hace sentir cada vez más distantes de los días en que empezamos o nos unimos inicialmente a la plataforma como usuarixs. Hemos definido algunos de los temas y todavía estamos recogiendo información sobre posibles mejoras. Otra cosa muy importante es limpiar, automatizar y mejorar todos los trabajos relacionados con el servidor. **Disroot** sigue robando más y más tiempo valioso de los administradores e introduciendo la estandarización, automatización, creando scripts y mejorando el trabajo diario nos ayudará a recuperar algo de ese tiempo que podríamos usar para mejorar aún más los servicios y nuestras contribuciones a los proyectos FLOSS.
<br><br>
Para seguir el progreso de la hoja de ruta, puedes chequear [nuestro tablero](<https://board.disroot.org/project/disroot-disroot/epic/2167>)

## Mejoras en el sistema de tickets de soporte

**Meaz** trabajó duro para poner orden en la cola de los tickets. Últimamente se había vuelto muy caótico, lo que afectaba nuestros tiempos de respuesta, además de causar que algunos tickets "se perdieran" en la pila. El resultado de la limpieza generó un procedimiento para su manejo, creó colas de tickets detalladas e introdujo turnos, donde cada semana alguien asume el rol de "ticket master", que será la persona de soporte de primera línea, delegando los tickets, asegurándose de que sean respondidos en un tiempo razonable y, lo más importante, de que se eviten los "perdidos". Definitivamente nos ayudará a ser más eficientes.

## Donaciones a proyectos FLOSS

Nuestra última donación a proyectos FLOSS fue en Mayo. Hemos estado separando una cantidad de dinero basada en el cálculo de la meta, y ahora es el momento de enviar este dinero a los proyectos. Esta vez hemos decidido donar a: **Hubzilla, ConverseJS, Etherpad, Framasoft, DavX5, recompensas Nextcloud** *(no lanzadas aún)*, **Conversations** y **Debian**. *(Las donaciones deberían ser realizadas en los próximos días)*
<br><br>
Aquí abajo está el resumen de las donaciones que realizamos este año a todos los proyectos. Para ver un resumen actualizado de nuestras finanzas, así como también el número de disrooters aportando a la causa y las donaciones hechas a proyectos FLOSS, revisa nuestra [página de donaciones](https://disroot.org/donate).

![](donated.png)

Gracias a todxs por su gran apoyo, y un gran saludo a todxs aquellxs desarrolladorxs de FLOSS que trabajan incontables horas para conseguir un software mejor y más abierto. ♥

## Nueva página principal

**Meaz** y **Antilopa** estuvieron muy ocupados trabajando en el nuevo look de nuestra página principal. Nos gusta muchísimo y esperamos que les guste también. Este es el primer paso en una serie de tareas en las que nos enfocamos en unificar los estilos, tanto del sitio web como de los servicios.

## Email opcional para restablecimiento de la contraseña

Una de las preguntas de soporte más recurrentes es sobre el restablecimiento de contraseñas. Muchxs de ustedes han creado una cuenta y en el momento en que fue aprobada han olvidado su contraseña, resultando en una cuenta bloqueada. Por mucho que no querramos recopilar direcciones de correo de terceros, parece ser la única manera. Así que, de ahora en adelante, durante el registro pueden elegir utilizar la dirección de correo electrónico de verificación (que normalmente se borra de nuestras bases de datos tras la aprobación/denegación) para ser utilizada como una dirección de correo de "restablecimiento de contraseña". También la pueden configurar en una etapa posterior a través de https://user.disroot.org, iniciando sesión y seleccionando la opción "Actualizar la información de la cuenta". Recuerden que esto es totalmente **Opcional** y si no necesitan ni quieren compartir con nosotros su dirección de correo electrónico externa, no tienen que hacerlo (mantengan sus contraseñas seguras y no necesitarán nada de eso).

## Nuestra instancia git

Durante un tiempo hemos estado discutiendo si trasladar nuestros repositorios git actuales, que alojamos con nuestrxs amigxs de [Fosscommunity India](https://git.fosscommunity.in), a otro lugar. Nos gusta realmente mucho la simplicidad de Gitea, especialmente cuando no usamos prácticamente ninguna característica extra que venga con Gitlab y no nos gusta la interfaz de usuarix. Mientras buscábamos un nuevo hogar, llegamos a la conclusión de que quizás auto-alojar nuestra propia instancia de git podría ser lo más beneficioso para nosotrxs, nuestro equipo de traducción/howto y, de hecho, cualquier persona dentro de la comunidad que esté buscando un hogar para su código. Todavía estamos trabajando en los últimos detalles antes de que salgamos en vivo, pero esperamos finalizarla y migrar el sitio web y las guías de **Disroot** a la nueva instancia de git esta semana.

Ya pueden revisarla, y si quieren registrarse, en https://git.disroot.org. Decidimos que el método para iniciar sesión fuera separado (no puedes utilizar tus credenciales de **Disroot**) porque pensamos que así estimularemos la cooperación de aquellxs que no son Disrooters, ya que tener que pasar por todo el esfuerzo de crear una cuenta de **Disroot** solo para enviar un reporte de error o un cambio no va a incentivar a las personas. Además, estamos considerando agregar inicio de sesión con Github/Gitlab para hacer que colaborar sea más sencillo para los que vienen de afuera.

## Próximamente...

En las últimas semanas **Fede** está llevando las riendas del trabajo sobre una nueva versión de nuestra Política de Privacidad que esperamos la haga más clara y detallada. **Meaz** está haciendo más modificaciones en el sitio web, mientras **Antilopa** está trabajando en unas guías de "estilo de la casa" apropiadas para las interfaces gráficas. **Muppeth** ha estado ocupado estas semanas mejorando, documentando y arreglando la configuración del servidor.
