---
title: 'Roadmap and new wave of FLOSS donations, git and new front page'
date: '07-11-2019 16:00'
media_order: roadmap.jpg
taxonomy:
  category: news
  tag: [disroot, news, git, services, donations, floss, roadmap]
body_classes: 'single single-post'

---
*(Sprint 44-45-46)*
<br><br>
This blogpost has been overdue for two months! Somehow, even though it was drafted it kept on being pushed down the priority list into the abyss of forgotten tasks. Time to dig it out and post a portion of things we've been working on lately.

## Roadmap Q4 2019

We start with improvements to our internal organizational structure. That's why we have decided to create a roadmap for the last quarter of this year. This, we hope, will enable us to plan our work better and have a clear vision and goals ahead. Our intention is to continue this way of planning in 2020 and beyond. The idea is to have two types of roadmaps:

  - **Quarterly** - where we set short term goals, focus on specific projects
  - **Half yearly** - In order to not loose the sight of the big picture, we want to set long term goals twice a year to better organize our work, goals and vision for the project.
<br><br>


We noticed that we often tend to de-rail our focus, or neglect some parts of the project as a consequence of limited resources and no clear plan/vision. Roadmaps will help us keep our attention towards a common goal. How well we'll do, time will tell.
<br><br>
**So what are we going to focus on the rest of this year?**
<br><br>
Between September and December, we want to finalize organizational and structural work we have already started working on: (re)structuring the internal organization, the way we work, interact, make decision, share responsibilities. This means thinking of best solutions, documenting them, improving our workflows and procedure as well as working out a more effective administration. Additionally we want to focus on small improvements on UX (user experience) and UI (user interface) specially during the onboarding process. Running **Disroot** for quite a while makes us feel more detached from the days we started or initially joined the platform as users. We have defined some of the topics and we are still gathering feedback on possible improvements. Another very important thing is cleaning up, automating and improving all server side related jobs. **Disroot** keeps stealing more and more precious time from admins and introducing standardization, automation, scripting and bettering daily work will help us get some of that time back which we could use to further enhance services and contribute our improvements upstream.
<br><br>
To follow the progress of the roadmap check our [Epic on taiga](https://board.disroot.org/project/disroot-disroot/epic/2167)

## Improvements in support ticketing system

**Meaz** worked hard to bring order to the ticket queue. Lately it had became very chaotic which affected our response times, as well as caused some tickets to 'get missing' in the pile. The result of the cleaning generated a procedure for its handling, created detailed ticket queues and introduced shifts, where each week someone takes the role of "ticket master" who will be the first line support person delegating tickets, making sure they are answered in reasonable time and most importantly prevent 'missing' ones. It will definitely help us to be more efficient.

## Donations to FLOSS

Our last donation to FLOSS projects was in May. We've been putting aside an amount of money based on the goal calculation, and now it's about time to send another batch of money to the projects. This time we have decided to donate to: **Hubzilla, ConverseJS, Etherpad, Framasoft, DavX5, Nextcloud bounties** *(not released yet)*, **Conversations and Debian**. *(donations should be paid out in coming days)*
<br><br>
Below is the overview of all donations we made to all the projects this year. For up to date overview of our finances, as well as number of disrooters sponsoring the cause and overview of donations we've made to FLOSS projects, check our [donation page](https://disroot.org/donate)

![](donated.png)

Thank you all for your great support, and big shoutout to all those FLOSS developers working countless hours for better, more open software ♥♥♥

## New Front page

**Meaz** and **Antilopa** were busy working on new refreshed look of our Front page. We like it very much and we hope you like it too. This is a first step in series of tasks where we focus on unifying the looks, both of the website as well as services.

## Opt-in password reset email

One of the most re-occurring support question is about password reset. Many of you have registered an account and by the time you got approved you have forgotten your password, resulting in a locked out account. As much as we do not want to collect third party email addresses it seems like the only way. So from now on, during the registration you can choose to use the verification email address (which is normally wiped upon approval/denial from our databases) to be used as a "reset password" email address. You can also set it at later stage via https://user.disroot.org by logging in and selecting "Update account information" option. Remember this is entirely **Optional** and if you do not need nor want to share with us your external email address, you do not have to do it (keep your passwords safe and you won't need any of that).

## Our git instance

For a while we have been discussing whether to move our current git repositories which we host at our friends at [Fosscommunity India](https://git.fosscommunity.in) elsewhere. We really like the simplicity of Gitea specially, when we did not use pretty much any extra features that come with Gitlab and are not fond of the UI. While looking for new home, we came to the conclusion that perhaps selfhosting our own git instance could be the most beneficial for us, our translation/howto crew and actually anyone within the community who is looking for home for their code. We are still working out last details before we go live, but we hope to finalize it and migrate the **Disroot**'s website and howtos to the new git instance this week.

You can already check it out and if you want, register at https://git.disroot.org We have decided on separate login method (you can't use your **Disroot** credentials) because we think that in order to stimulate cooperations from non-disrooters, having to go through entire effort of creating **Disroot** account just to submit a bug report or commit is not going to encourage people. Furthermore, we are considering adding github/gitlab logins to further make it easier for outsiders to collaborate.

## Coming up...

Past weeks **Fede** is taking a lead in working on a new version of our current Privacy Policy which we hope will make the policy more clear and more detailed. **Meaz** is doing more website tweaks, while **Antilopa** is working on a proper "house style" GUI guides. **Muppeth** has been busy last weeks on improving, documenting and fixing server setup.
