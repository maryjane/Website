---
title: 'Mail storage'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

<br>
With your **Disroot** account, you get 1GB of FREE storage for your emails.

However, it is possible to extend your E-mail storage from 1GB **up to 10GB** for the cost of 0.15 euro per GB per month, paid yearly (VAT not included).

<a class="button button1" href="https://disroot.org/forms/extra-storage-mailbox">Request Extra Mailbox Storage</a>

---      

# Mail storage

![email](logo_email.png?resize=150)
