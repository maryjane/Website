---
title: 'Almacenamiento para el Correo'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

<br>
Con tu cuenta de **Disroot**, obtienes 1GB de almacenamiento gratuito para el buzón.

Sin embargo, es posible ampliar el espacio del correo de 1GB **hasta 10GB** por 0.15 euro mensuales por GB, abonado anualmente (IVA no incluido).

<a class="button button1" href="https://disroot.org/forms/extra-storage-mailbox">Solicitar Almacenamiento Extra para el Buzón</a>

---      

# Almacenamiento de Correo

![email](logo_email.png?resize=150)
