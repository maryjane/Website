---
title: 'Cloudspeicher'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Cloudspeicher

![cloud](logo_cloud.png?resize=150)

---      

<br>
In Deinem **Disroot** Account sind 2 GB FREIER Speicher enthalten.

Natürlich ist es möglich, Deinen Cloud-Speicherplatz von 2 GB **auf 14, 29 oder 54 GB** zu erweitern, zum Preis von 0,15 EUR pro GB pro Monat (VAT nicht enthalten).

Du kannst Dich dabei auch dafür entscheiden, einen Teil oder alles Deines zusätzlichen Speichers als E&#8209;Mailspeicher zu nutzen *(achte darauf, dies im Kommentarbereich des Antragsformulars genauer zu spezifizieren)*.

<a class="button button1" href="https://disroot.org/forms/extra-storage-space">Zusätzlichen Cloudspeicher beantragen</a>
