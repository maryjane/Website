---
title: 'Almacenamiento en la Nube'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Almacenamiento en la Nube

![cloud](logo_cloud.png?resize=150)

---      

<br>
Con tu cuenta de **Disroot**, obtienes 2GB de almacenamiento gratuito.

Sin embargo, es posible ampliarlo de 2GB **a 14, 29 o 54GB** por 0.15 euros mensuales por GB (IVA no incluido).

También puedes elegir utilizar una parte o todo el espacio adicional para ampliar el almacenamiento del buzón de correo *(asegúrate de especificarlo en la sección de comentarios del formulario)*.

<a class="button button1" href="https://disroot.org/forms/extra-storage-space">Solicitar Almacenamiento Extra para la Nube</a>
