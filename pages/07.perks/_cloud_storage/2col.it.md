---
title: 'Archiviazione sul cloud'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Archiviazione sul cloud

![cloud](logo_cloud.png?resize=150)

---      

<br>
Con il tuo account **Disroot** hai a disposizione per l'archiviazione 2GB. 

È comunque possibile estendere il proprio spazio di archiviazione da 2GB a **14, 29 o 54GB** al costo di 0,15 euro per GB al mese (IVA esclusa). 

Puoi anche scegliere di utilizzare parte o tutto lo spazio aggiuntivo per il servizio di posta elettronica *(in questo caso, assicurati di specificarlo nella sezione commenti del modulo di richiesta)*. 

<a class="button button1" href="https://disroot.org/forms/extra-storage-space">Richiedi spazio di archiviazione extra</a>
