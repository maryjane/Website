---
title: Contribuer
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
published: true
---

![](bug.png?resize=45,45&classes=pull-left)
# Comment puis-je aider?

Il y a plusieurs façons de devenir actif et de contribuer au projet **Disroot**.
