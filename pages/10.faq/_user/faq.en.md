---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "USER"
section_number: "500"
faq:
    -
        question: "I forgot my password. How can I reset it?"
        answer: "<p>In order to reset your password, you need to have security questions and/or a recovery email set up. If this is the case, then go <a href='https://user.disroot.org/' target='_blank'>here </a> and click on 'Forgotten password'.</p>
        <p>
        If you haven't set any of these options, then there's nothing else you can do, but register a new account.</p>"
    -
        question: "My account was approved but I can’t login. Why?"
        answer: "<p>It is very likely that you’re incorrectly entering:</p>
        <ul class=disc>
        <li>your username (remember it’s just the username, without the domain), or...</li>
        <li>your password (remember it’s case sensitive).</li>
        </ul>"
    -
        question: "I get an <i>'Invalid private key for encryption app'</i> message. Why?"
        answer: "<p>It is probably because you've changed your password recently. Since Nextcloud uses the user password to generate the encryption keys, it’s necessary to regenerate them from your new password. To do so, you must:</p>
        <ol>
        <li>log in to the cloud </li>
        <li>select <b>Personal</b> from the menu </li>
        <li>scroll down to the <b>Basic encryption module</b> and type in your old password </li>
        <li>then the new one and </li>
        <li>finally click <b>Update Private Key Password</b></li></ol>

        <p>After login out and back into the cloud again you should see your files and the message should have disappeared.</p>

        <p>If you don't remember your old password, it's still possible to reset the account but it won't be possible to recover any files on the cloud as they are encrypted with the old key. What you have to do is to remove all files from Nextcloud (this does not include calendars, contacts, etc., just files), and to contact us (support@disroot.org). We will then proceed wiping the key so the new key pair based on your current password can be re-generated automatically upon new login.</p>"
    -
        question: "Does my Disroot account expire?"
        answer: "<p> No. We do not to expire unused accounts. However we appreciate when people take responsibility for their account, and its impact on the <b>Disroot</b> resources, and request account deletion when they decide not to use our services any longer.</p>"
    -
        question: "How can I delete my Disroot account?"
        answer: "<p>To delete your <b>Disroot</b> account log in to <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> and select <b>'Delete My Account'</b>.</p> <p>Accounts and their data are wiped on daily.</p>"
    -
        question: "How can I change my password?"
        answer: "<p>Log in to <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> and select <b>'Change Password'</b>.</p>
        <p>You will also need to change your password on <b>Nextcloud</b> to be able to access your files, which are encrypted using your password. To do it, follow the steps as described in the answer to <i>'I get an 'Invalid private key for encryption app' message'</i> or check <a href='https://howto.disroot.org/en/tutorials/user/account/administration/password' target='_blank'>this tutorial.</a></p>"

---
