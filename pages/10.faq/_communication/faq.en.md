---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "COMMUNICATION"
section_number: "100"
faq:
    -
        question: "How do I use Disroot's IRC in XMPP rooms?"
        answer: "<p><b>Disroot</b> provides so-called IRC gateway. This means you can join any IRC room hosted on any IRC server. This is the basic address schema:</p>
        <ol>
        <li>To join an IRC room: <b>#room%irc.domain.tld@irc.disroot.org</b></li>
        <li>To add an IRC contact: <b>contactname%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Where <b>#room</b> is the IRC room you want to join and <b>irc.domain.tld</b> is the irc server address you want to join. Make sure to leave <b>@irc.disroot.org</b> as it is because it's the IRC gateways address.</p>"

---
