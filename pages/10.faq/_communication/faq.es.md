---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SOBRE COMUNICACIÓN"
section_number: "100"
faq:
    -
        question: "¿Cómo puedo utilizar el IRC de Disroot en las salas XMPP?"
        answer: "<p><b>Disroot</b> proporciona la llamada puerta de enlace IRC. Esto significa que puedes unirte a cualquier sala IRC hospedada en cualquier servidor IRC. Este es el esquema de dirección básico:</p>
        <ol>
        <li>Para unirte a una sala IRC: <b>#sala%irc.domain.tld@irc.disroot.org</b></li>
        <li>Para agregar un contacto IRC: <b>nombre_del_contacto%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Donde <b>#sala</b> es la sala IRC a la que quieres unirte y <b>irc.domain.tld</b> es la dirección del servidor. Asegúrate de dejar <b>@irc.disroot.org</b> como está porque es la dirección del enlace IRC.</p>"

---
