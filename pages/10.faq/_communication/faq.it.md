---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "COMUNICAZIONE"
section_number: "100"
faq:
    -
        question: "Come posso usare l'IRC di Disroot nelle stanze XMPP?"
        answer: "<p><b>Disroot</b> offre un servizio di IRC gateway. Questo significa che dal protocollo XMPP puoi accedere a qualsiasi stanza su server IRC. Per poter accedere ti basta utilizzare il seguente schema:</p>
        <ol>
        <li>Per accedere ad una stanza IRC: <b>#room%irc.domain.tld@irc.disroot.org</b></li>
        <li>Per aggiungere un contatto IRC: <b>contactname%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Dove <b>#room</b> è la stanza irc alla quale si vuole accedere e <b>irc.domain.tld</b> è l'indirizzo del server sul quale la stanza è ospitata. Ricordati di non modificare <b>@irc.disroot.org</b> in quanto è  l'indirizzo dell'IRC gateway.</p>"

---
