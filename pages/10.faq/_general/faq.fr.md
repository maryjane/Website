---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "GENERAL"
section_number: "300"
faq:
    -
        question: "Que veut dire Disroot?"
        answer: "<p><b>Dis'root'</b> veut dire Déraciner: Enlever les racines ou par les racines ; donc, séparer d'une base ; déraciner; extirper.</p>"
    -
        question: "Le projet Disroot durera-t-il ?"
        answer: "<p>Les administrateurs et responsables de <b>Disroot</b> l'utilisent quotidiennement et en font leur principal outil de communication. Nous avons l'intention de le faire durer très longtemps. <a href='https://forum.disroot.org/t/will-disroot-last/101' target='_blank'>Consulter le Forum pour la réponse longue</a></p>"
    -
        question: "Combien de personnes utilisent Disroot?"
        answer: "<p>Nous ne gardons pas la trace des utilisateurs actifs, nous ne pouvons donc pas répondre à cette question. D'ailleurs, nous considérons que ce n'est en aucun cas une façon de mesurer l'état de santé de la plateforme. Les nombres d'utilisateurs sont susceptibles d'être manipulés pour de nombreuses raisons, par exemple pour satisfaire les investisseurs. Puisque les objectifs de la plate-forme n'ont rien à voir avec tout cela, nous ne voyons pas l'intérêt de donner des statistiques fausses ou manipulées à ce sujet.</p>"

---
